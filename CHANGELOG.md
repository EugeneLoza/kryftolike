# 1.10.683
* 5 new structures (total 61)
* Bugfix: some of the structures may have generated areas not connected to the dungeon

# 1.10.670
* How to Play (aka tutorial) page accessible from Main Menu
* 8 new biomes:
* * Metal: Long not organized corridors
* * Black: Organized corridors with little treasure or monsters
* * Vault: Very organized corridors with larger rooms
* * Prison: Long passages with multiple nooks
* * Mossy: Large open areas
* * Three variants of structure biomes
* Structures - 56 premade building-like objects that can appear on the map
* 6 variants of Player character image, randomized at map start
* More monsters variants
* Sigils: player can pass through them, but monsters can't; 3 variants
* Rework map tileset - more diversity
* 221 new hints and jokes
* Treasure now is rendered on the zoom-out map almost always, not only when close to the Player position
* Mouse wheel also zooms in/out
* Do not treat first playthrough as high score
* More debug options (in debug build only): F1 - teleport for free; F2 - display all generated map; F3 - Generate a huge map around the Player
* Modding tool to visually create structures that can be placed on the map
* Fix deprecated way of sound handling; upgrade Castle Game Engine version (optimiztions, bugfixes)

# 1.9.552
* Upgrade CGE: Fix handling of FreeType library when the game was put into a folder with non-ASCII symbols and username also contained non-ASCII symbols
* Small game optimizations

# 1.9.542
* Fix APK installation on Android 12 (Castle Engine upgrade, thanks to Michalis Kamburelis)
* +18 hints/jokes
* Free achievements notifications (tiny optimization)
* Update game metadata, mostly for Debian Package

# 1.9.534
* Monsters now have a 12% chance to skip movement each turn, that gives player a chance to increase distance and subsequently shake them off if able to control the situation for long enough.
* +45 new hints and jokes.
* Removed 1 almost-duplicate hint.

# 1.9.525
* Upgrade Castle Game Engine version. Major improvement: Enable optimizations on aarch processors (some new Android devices).

# 1.9.524
* Fix a bug introduced in previous version: the score wasn’t added for exploration.
* Display safety timeout on compass (the amount of turns until monsters will start moving to your location).
* Increase safety timeouts for all cases, should help with “close encounters”.

# 1.9.520
* Significantly more efficient render (3x FPS on zoom-out)
* Significantly improved look-around algorithm (both faster and doesn’t make “holes”).
* Fall back to Vampyr library, not FpImage in case LibPng is not available (faster loading on some old Android devices).

# 1.8.507
* Fixed map rendering on non-16:9 screens

# 1.8.505
* Fixed Debian/Ubuntu package building and it has been updated to the latest version.
* Also minor optimizations in configuration saving/loading.

# 1.8.496
* Fix a typo
* +2 more hints/jokes
* Fix a pixel in monster texture
* Workaround crash on some ancient Android devices due to LibPng error (fall back to slower FpImg)

# 1.8.490
* Better buttons in menus and options
* More readable texts in menus and credits (thanks to u/Ms_ellery for the hint)
* Castle Game Engine upgrade: faster loading on Android
* Fix rare crash on old Android devices
* Fix bug when player could teleport straight into treasure
* Executable flag is now automatically set on Linux
* +2 hints

# 1.7.471
* Android version is now properly signed - fixes bugs with highscores and achievements lost on upgrading the game
* 7 more hints and jokes
* F5 key now takes screenshots on desktop

# 1.7.464
* Improve Debian package metadata
* Add instruction on installing on Debian/Ubuntu
* Add several more hints and jokes

# 1.7.454
* Improve in-game loading speed
* 2 more hints
* Debian Package

# 1.7.445
* Increase map performance
* Better long-range movement for Player
* Change Fullscreen mode in Options
* Vibration on Android + turn it On/Off in Options
* Fix bug when the game was not always deleted manually
* Show game version in Options menu
* Upgrade Castle Game Engine and code cleanup
* More hints and jokes, as always :)

# 1.6.419
* Option to start a new game (delete save)
* Improve Readme
* A few minor fixes and improvements

# 1.6.414
* Fixed movement maps on Android (now they can be any size)
* Improved movement map generation speed
* Separated Player’s and Enemies’ movement maps - now player can click-to-move at any time (especially important for Android)
* (as usually) more hints and jokes

# 1.6.398
* Save game feature on all platforms
* Movement map takes less memory
* If Player presses/clicks something movement is interrupted
* Improve the menus
* Enemies can now pass over the treasure (won’t get stuck behind it)
* More hints and jokes:)

# 1.5.362
* Significantly improved tileset (thanks to advice from Chaigidel@reddit) - now it’s much easier to see the difference between passages and walls.
* Optimized pathfinding algorithm for player - now it won’t cause slowdowns on low-end Android devices and unnecessary battery drain
* More hints and jokes :)

# 1.4.357
* A hotfix for out-of-memory crash on Android devices.

# 1.4.355
* Minor fixes to Readme + a few more hints

# 1.4.349
* Tap on the map to move to the location
* Fix a very rare bug that might have theoretically caused the dungeon to be a stub
* Splash screen is no longer blurry
* More hints

# 1.3.327
* Added more hints in the game and improved readme.

# 1.2.319
* Fixed a critical but very rare bug with map generation. Now Player won’t be able to accidentally teleport into the wall, neither monsters or treasure could spawn there.
* Many more hints.

# 1.1.309
* Fix High Score being overwritten by the latest score
* Needles sway a bit more
* Fix internal version of the project
* Fix and improve Readme

# 1.0.304
* First release (FFS2021 jam version)
