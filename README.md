# Kryftolike

Explore infinite maze looking for treasure and hiding from monsters.

## About the game

Earn as much points as you can, exploring an infinite world featuring Monsters and Treasure!

You cannot fight monsters, but you can teleport away from the danger. To be able to teleport collect 6 treasures of different colors.

Be careful, danger awaits around every corner.

### Controls

* WASD or arrow keys - Move
* Tab or T - Teleport (only when enemies are nearby)
* Spacebar or 1 - Wait 1 turn
* 0 - Wait for 25 turns (lay low)
* Escape - Menu
* F11 - Toggle Full Screen mode
* F5 - Take a screenshot
* +/- - Zoom in/out

Alternatively, all of the above are accessible through a control panel on the right side of the screen.

You can tap/click on the map to move to the location. In case there are enemies around, you'll move one step at a time.

## Installation

### Windows

No installation required - just extract the game into one folder and play.

### Linux

No installation required - just extract the game into one folder and play.

In some situations you might also need to set "executable" flag for the binary (`chmod +x kryftolike` or through file properties in the file manager).

You need the following libraries installed to play the game:

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* You also need X-system, GTK at least version 2 and OpenGL drivers for your videocard, however, you are very likely to have those already installed :)

Alternatively on Debian-based systems like Ubuntu you can download DEB package and install it through a package manager or by `dpkg -i kryftolike-*-linux-x86_64.deb` command.

### Android

Download and install the APK. The game requires Android 4.2 and higher.

Note that you may need to allow installation of third-party APKs in phone/tablet Settings.

## Credits

Game by EugeneLoza

Created in Castle Game Engine (https://castle-engine.io/).

Graphics by RLTiles, Lorc, Skitterphoto, Ian L, Wyrmheart

Font by Wojciech Kalinowski

Sound & Music by SubspaceAudio, Philippe Groarke

## Links

Download the latest version: https://decoherence.itch.io/kryftolike

Source code: https://gitlab.com/EugeneLoza/kryftolike (GPLv3)
