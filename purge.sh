#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

rm -f *.log
find -type d -name 'backup' -prune -exec rm -rf {} \;
rm -f *.dbg
rm -f kryftolike
rm -f kryftolike.exe
rm -rf castle-engine-output/standalone
rm -f *.res
