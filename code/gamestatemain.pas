{  Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStateMain;

interface

uses Classes,
  CastleComponentSerialize, CastleUiControls, CastleControls,
  CastleKeysMouse, CastleTimeUtils,
  BatchedMap, Global;

const
  BlockControlsDuration = 1; //seconds
  LastDangerTimeout = 10; //seconds
  AchievementShowDuration = 15; //seconds
  SaveGameTimeout = 60; //seconds

type
  TStateMain = class(TCastleView)
  strict private
    AskToInterrupt: Boolean;
    LastDanger, BlockControlsStart: TTimerResult;
    BlockControls: Boolean;
    Map: TBatchedMap;
    TreasureLabels: array[0..5] of TCastleLabel;
    LabelTeleport: TCastleLabel;
    TreasureNeedle, EnemyNeedle: TCastleImageControl;
    DangerSensor: TCastleImageControl;
    LabelScore: TCastleLabel;
    LabelHint: TCastleLabel;
    LabelSafetyTimeout, LabelSafetyTimeoutShadow: TCastleLabel;

    GameOverGroup: TCastleUserInterface;
    LabelHighScoreShadow: TCastleLabel;
    LabelTilesExploredShadow, LabelTilesExplored: TCastleLabel;
    LabelTreasuresFoundShadow, LabelTreasuresFound: TCastleLabel;
    LabelTotalScoreShadow, LabelTotalScore: TCastleLabel;

    AchievementsVerticalGroup: TCastleVerticalGroup;
    LastAchievement: TTimerResult;

    procedure NewAchievement(const AchievementUrl: String);
    procedure PressTeleport(Sender: TObject);
    procedure PressMenu(Sender: TObject);
    procedure PressWaitLong(Sender: TObject);
    procedure PressWait(Sender: TObject);
    procedure PressZoomIn(Sender: TObject);
    procedure PressZoomOut(Sender: TObject);
    procedure PressDown(Sender: TObject);
    procedure PressUp(Sender: TObject);
    procedure PressLeft(Sender: TObject);
    procedure PressRight(Sender: TObject);
    procedure PlayerStateChanged(Sender: TObject);
    function PlayerCanMove: Boolean;
    procedure MapClick(const ClickV: TCoordinateVector);
  public
    DoLoadGame: Boolean;
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Stop; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
    procedure UpdateUi;
  end;

var
  StateMain: TStateMain;

implementation

uses
  SysUtils, Math,
  CastleWindow, CastleColors,
  GamePlayer, GameMap, GameHints, GameContext,
  GameStateInGameMenu, GameAchievements, GameSave, GameSounds;

{ TStateMain ----------------------------------------------------------------- }

constructor TStateMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/state_main.castle-user-interface';
  DesignPreload := true;
end;

procedure TStateMain.Start;
var
  Background: TCastleImageControl;
  ButtonTeleport, ButtonMenu, ButtonWaitLong, ButtonWait,
    ButtonZoomIn, ButtonZoomOut, ButtonDown, ButtonUp,
    ButtonLeft, ButtonRight: TCastleButton;
begin
  inherited;

  OnNewAchievement := @NewAchievement;

  LastDanger := Timer;
  BlockControls := false;
  BlockControlsStart := Timer;

  TreasureLabels[0] := DesignedComponent('Treasure1') as TCastleLabel;
  TreasureLabels[1] := DesignedComponent('Treasure2') as TCastleLabel;
  TreasureLabels[2] := DesignedComponent('Treasure3') as TCastleLabel;
  TreasureLabels[3] := DesignedComponent('Treasure4') as TCastleLabel;
  TreasureLabels[4] := DesignedComponent('Treasure5') as TCastleLabel;
  TreasureLabels[5] := DesignedComponent('Treasure6') as TCastleLabel;
  LabelTeleport := DesignedComponent('LabelTeleport') as TCastleLabel;
  LabelScore := DesignedComponent('LabelScore') as TCastleLabel;
  LabelHint := DesignedComponent('LabelHint') as TCastleLabel;
  LabelSafetyTimeout := DesignedComponent('LabelSafetyTimeout') as TCastleLabel;
  LabelSafetyTimeoutShadow := DesignedComponent('LabelSafetyTimeoutShadow') as TCastleLabel;

  GameOverGroup := DesignedComponent('GameOverGroup') as TCastleUserInterface;
  LabelHighScoreShadow := DesignedComponent('LabelHighScoreShadow') as TCastleLabel;
  LabelTilesExploredShadow := DesignedComponent('LabelTilesExploredShadow') as TCastleLabel;
  LabelTilesExplored := DesignedComponent('LabelTilesExplored') as TCastleLabel;
  LabelTreasuresFoundShadow := DesignedComponent('LabelTreasuresFoundShadow') as TCastleLabel;
  LabelTreasuresFound := DesignedComponent('LabelTreasuresFound') as TCastleLabel;
  LabelTotalScoreShadow := DesignedComponent('LabelTotalScoreShadow') as TCastleLabel;
  LabelTotalScore := DesignedComponent('LabelTotalScore') as TCastleLabel;
  GameOverGroup.Exists := false;

  AchievementsVerticalGroup := DesignedComponent('AchievementsVerticalGroup') as TCastleVerticalGroup;
  LastAchievement := Timer;

  ButtonTeleport := DesignedComponent('ButtonTeleport') as TCastleButton;
  ButtonMenu := DesignedComponent('ButtonMenu') as TCastleButton;
  ButtonWaitLong := DesignedComponent('ButtonWaitLong') as TCastleButton;
  ButtonWait := DesignedComponent('ButtonWait') as TCastleButton;
  ButtonZoomIn := DesignedComponent('ButtonZoomIn') as TCastleButton;
  ButtonZoomOut := DesignedComponent('ButtonZoomOut') as TCastleButton;
  ButtonDown := DesignedComponent('ButtonDown') as TCastleButton;
  ButtonUp := DesignedComponent('ButtonUp') as TCastleButton;
  ButtonLeft := DesignedComponent('ButtonLeft') as TCastleButton;
  ButtonRight := DesignedComponent('ButtonRight') as TCastleButton;

  ButtonTeleport.OnClick := @PressTeleport;
  ButtonMenu.OnClick := @PressMenu;
  ButtonWaitLong.OnClick := @PressWaitLong;
  ButtonWait.OnClick := @PressWait;
  ButtonZoomIn.OnClick := @PressZoomIn;
  ButtonZoomOut.OnClick := @PressZoomOut;
  ButtonDown.OnClick := @PressDown;
  ButtonUp.OnClick := @PressUp;
  ButtonLeft.OnClick := @PressLeft;
  ButtonRight.OnClick := @PressRight;

  TreasureNeedle := DesignedComponent('TreasureNeedle') as TCastleImageControl;
  EnemyNeedle := DesignedComponent('EnemyNeedle') as TCastleImageControl;
  DangerSensor := DesignedComponent('DangerSensor') as TCastleImageControl;

  Background := DesignedComponent('Background') as TCastleImageControl;
  Map := TBatchedMap.Create(FreeAtStop);
  Map.Zoom := 1;
  Map.OnMapClick := @MapClick;
  Map.Height := Background.EffectiveHeight;
  Map.Width := Background.EffectiveWidth;
  Background.InsertBack(Map);

  if DoLoadGame then
    LoadGame
  else
  begin
    DeleteSaveGame;
    Player := TPlayer.Create;
    DungeonMap := TMap.Create;
    DungeonMap.GeneratePlayer;
    Player.OnStateChanged := @PlayerStateChanged;
    Player.Alert(-1);
    Player.TryMove(0, 0);
  end;

  Player.OnStateChanged := @PlayerStateChanged;
  Map.CenterX := Player.X;
  Map.CenterY := Player.Y;
  UpdateUi;
  PlayerStateChanged(Player);

  Music('game_ambient');
end;

procedure TStateMain.Stop;
begin
  inherited;
  FreeAndNil(Player);
  FreeAndNil(DungeonMap);
  OnNewAchievement := nil;
end;

function TStateMain.Press(const Event: TInputPressRelease): Boolean;
begin
  AskToInterrupt := true;
  Result := inherited;
  if Result then
    Exit;

  if Event.EventType = itMouseWheel then
  begin
    if Event.MouseWheelScroll > 0 then
      PressZoomIn(nil)
    else
      PressZoomOut(nil);
  end;

  if Event.EventType = itKey then
    case Event.Key of
      keyW, keyArrowUp, keyNumpad8: PressUp(nil);
      keyS, keyArrowDown, keyNumpad2: PressDown(nil);
      keyA, keyArrowLeft, keyNumpad4: PressLeft(nil);
      keyD, keyArrowRight, keyNumpad6: PressRight(nil);
      keyTab, keyT: PressTeleport(nil);
      key1, keySpace: PressWait(nil);
      key0: PressWaitLong(nil);
      keyEscape: PressMenu(nil);

      {$ifdef DEBUG}
      keyF1: begin Player.TryTeleport(true); UpdateUi; end;
      keyF2: begin DungeonMap.DebugSetAllVisible; UpdateUi; end;
      keyF3: begin DungeonMap.GenerateChunksAroundPlayer(false, Trunc(Map.RenderRect.Width / 32 / 2) + 1); UpdateUi; end;
      {$endif}

      keyPlus, keyNumPadPlus: PressZoomIn(nil);
      keyMinus, keyNumPadMinus: PressZoomOut(nil);
      else ; // remove 6060 warning
    end;
end;

function TStateMain.PlayerCanMove: Boolean;
begin
  if BlockControls then
    if BlockControlsStart.ElapsedTime > BlockControlsDuration then
      BlockControls := false;
  Result := not BlockControls;
end;

procedure TStateMain.MapClick(const ClickV: TCoordinateVector);
const
  PauseMilliseconds = 100;
var
  TimerStart: TTimerResult;
  RemainingFrameTime: Integer;
  GoToVector: TCoordinateVector;
begin
  if Player.IsAlive then
  begin
    AskToInterrupt := false;
    if DungeonMap.GetVisible(ClickV.X, ClickV.Y) then
      // Just go there
      GoToVector := ClickV
    else
      // Find a nearest spot to go to
      GoToVector := DungeonMap.FindNearestGoToDestination(ClickV);
    Player.PlayerMovementMap.BuildMovementMap(GoToVector, MaxInt, false, CoordinateVector(Player.X, Player.Y), true);
    while Player.FollowMovementMap do
    begin
      TimerStart := Timer;
      UpdateUi;
      RemainingFrameTime := PauseMilliseconds - Round(1000 * TimerSeconds(Timer, TimerStart));
      if RemainingFrameTime > 0 then
        Sleep(RemainingFrameTime);
      if AskToInterrupt or Player.EnemiesCanSeePlayer then
        Break;
    end;
    UpdateUi;
  end else
    PressMenu(Self);
end;

procedure TStateMain.PressTeleport(Sender: TObject);
begin
  if PlayerCanMove then
  begin
    Player.TryTeleport(false);
    UpdateUi;
  end;
end;

procedure TStateMain.NewAchievement(const AchievementUrl: String);
begin
  LastAchievement := Timer;
  AchievementsVerticalGroup.InsertFront(UserInterfaceLoad(AchievementUrl, AchievementsVerticalGroup));
end;

procedure TStateMain.PressMenu(Sender: TObject);
begin
  Sound('ui_click');
  Container.PushView(StateInGameMenu);
end;
procedure TStateMain.PressWaitLong(Sender: TObject);
const
  WaitLongTurns = 20;
  PauseMilliseconds = 20;
var
  I: Integer;
  TimerStart: TTimerResult;
  RemainingFrameTime: Integer;
begin
  if not Player.EnemiesCanSeePlayer then
    Sound('ui_click');
  I := 0;
  while (not Player.EnemiesCanSeePlayer) and (I < WaitLongTurns) do
  begin
    TimerStart := Timer;
    Player.TryMove(0, 0);
    UpdateUi;
    Inc(I);
    RemainingFrameTime := PauseMilliseconds - Round(1000 * TimerSeconds(Timer, TimerStart));
    if RemainingFrameTime > 0 then
      Sleep(RemainingFrameTime);
  end;
end;
procedure TStateMain.PressWait(Sender: TObject);
begin
  if not Player.EnemiesCanSeePlayer then
  begin
    Sound('ui_click');
    Player.TryMove(0, 0);
  end;
end;
procedure TStateMain.PressZoomIn(Sender: TObject);
begin
  if Map.Zoom < 8 then
    if Map.Zoom < 1 then
    begin
      Sound('ui_click');
      Map.Zoom *= 2
    end else
    begin
      Sound('ui_click');
      Map.Zoom += 1;
    end;
  Window.Invalidate;
  Application.ProcessAllMessages;
end;
procedure TStateMain.PressZoomOut(Sender: TObject);
begin
  if Map.Zoom > 1/32 then
    if Map.Zoom <= 1 then
    begin
      Sound('ui_click');
      Map.Zoom /= 2
    end else
    begin
      Sound('ui_click');
      Map.Zoom -= 1;
    end;
  Window.Invalidate;
  Application.ProcessAllMessages;
end;
procedure TStateMain.PressDown(Sender: TObject);
begin
  if PlayerCanMove then
  begin
    Player.TryMove( 0, -1);
    UpdateUi;
  end;
end;
procedure TStateMain.PressUp(Sender: TObject);
begin
  if PlayerCanMove then
  begin
    Player.TryMove( 0, +1);
    UpdateUi;
  end;
end;
procedure TStateMain.PressLeft(Sender: TObject);
begin
  if PlayerCanMove then
  begin
    Player.TryMove(-1,  0);
    UpdateUi;
  end;
end;
procedure TStateMain.PressRight(Sender: TObject);
begin
  if PlayerCanMove then
  begin
    Player.TryMove(+1,  0);
    UpdateUi;
  end;
end;

procedure TStateMain.UpdateUi;
var
  I: Integer;
  CanTeleport: Boolean;

  procedure ClearAndFreeControls(const A: TCastleUserInterface);
  var
    C: TCastleUserInterface;
  begin
    while A.ControlsCount > 0 do
    begin
      C := A.Controls[0];
      A.RemoveControl(C);
      C.Free;
    end;
  end;

begin
  CanTeleport := true;
  for I := 0 to 5 do
  begin
    TreasureLabels[I].Caption := IntToStr(Player.TreasureInventory[I]);
    if Player.TreasureInventory[I] = 0 then
    begin
      TreasureLabels[I].Color := Red;
      CanTeleport := false;
    end else
      TreasureLabels[I].Color := Lime;
  end;
  if CanTeleport then
  begin
    if Player.EnemiesCanSeePlayer then
      LabelTeleport.Color := Orange
    else
      LabelTeleport.Color := White;
  end else
    LabelTeleport.Color := Gray;

  if Player.TreasureVector.Length > 0 then
    TreasureNeedle.Rotation := ArcTan2(-Player.TreasureVector.X, Player.TreasureVector.Y);
  TreasureNeedle.Rotation := TreasureNeedle.Rotation + (Xorshift.Random - 0.5) * 0.07;
  if Player.EnemyVector.Length > 0 then
    EnemyNeedle.Rotation := ArcTan2(-Player.EnemyVector.X, Player.EnemyVector.Y);
  EnemyNeedle.Rotation := EnemyNeedle.Rotation + (Xorshift.Random - 0.5) * 0.07;
  if (Player.EnemiesCanSeePlayer) then
    DangerSensor.Color := Red
  else
  if (Player.AlertTimeout > 0) then
    DangerSensor.Color := Yellow
  else
    DangerSensor.Color := Lime;

  LabelScore.Caption := Player.Score.ToString;

  if Player.AlertTimeout > 0 then
  begin
    LabelSafetyTimeout.Exists := true;
    LabelSafetyTimeoutShadow.Exists := true;
    if Player.SafetyTimeout > 0 then
    begin
      LabelSafetyTimeout.Caption := Player.SafetyTimeout.ToString;
      LabelSafetyTimeoutShadow.Caption := Player.SafetyTimeout.ToString;
    end else
    begin
      LabelSafetyTimeout.Caption := '0';
      LabelSafetyTimeoutShadow.Caption := '0';
    end;
  end else
  begin
    LabelSafetyTimeout.Exists := false;
    LabelSafetyTimeoutShadow.Exists := false;
  end;

  if Player.IsAlive then
    GameOverGroup.Exists := false
  else
  begin
    GameOverGroup.Exists := true;
    LabelHighScoreShadow.Exists := Player.IsHighScore;
    LabelTilesExploredShadow.Caption := 'Tiles Explored : ' + Player.TilesDiscovered.ToString;
    LabelTilesExplored.Caption := LabelTilesExploredShadow.Caption;
    LabelTreasuresFoundShadow.Caption := 'Treasures Found : ' + Player.TreasureFound.ToString;
    LabelTreasuresFound.Caption := LabelTreasuresFoundShadow.Caption;
    LabelTotalScoreShadow.Caption := 'Total Score : ' + Player.Score.ToString;
    LabelTotalScore.Caption := LabelTotalScoreShadow.Caption;
  end;

  Map.CenterX := Player.X;
  Map.CenterY := Player.Y;

  if Player.EnemiesCanSeePlayer then
  begin
    if LastDanger.ElapsedTime > LastDangerTimeout then
    begin
      BlockControls := True;
      BlockControlsStart := Timer;
      SaveGame;
    end;
    LastDanger := Timer;
  end;

  if LastAchievement.ElapsedTime > AchievementShowDuration then
    ClearAndFreeControls(AchievementsVerticalGroup);

  Window.Invalidate;
  Application.ProcessAllMessages;
end;

procedure TStateMain.PlayerStateChanged(Sender: TObject);
var
  PlayerContext: TContext;
begin
  PlayerContext := Player.Context;
  LabelHint.Caption := GetRandomHint(PlayerContext);
  PlayerContext.Free;
  if SecondsSinceLastSave > SaveGameTimeout then
    SaveGame;
  // Somebody else will call UpdateUi
end;

end.
