{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{}
unit GameSave;

{$mode objfpc}{$H+}

interface

uses
  DOM;

const
  SaveGameVersion = 1;

procedure SaveGame;
procedure LoadGame;
function SaveGameExists: Boolean;
procedure DeleteSaveGame;
function SecondsSinceLastSave: Single;

implementation
uses
  SysUtils,
  CastleConfig, CastleXmlUtils, CastleTimeUtils, CastleLog,
  GamePlayer, GameMap;

var
  LastSaveTimer: TTimerResult;

procedure SaveGame;
var
  Element: TDOMElement;
  T: TTimerResult;
begin
  if (Player = nil) or (not Player.isAlive) then
    Exit;
  T := Timer;
  UserConfig.DeletePath('save_game');
  Element := UserConfig.MakePathElement('save_game');
  Player.Save(Element);
  DungeonMap.Save(Element);
  UserConfig.SetValue('savegame_version', SaveGameVersion);
  UserConfig.MarkModified;
  UserConfig.Save;
  WriteLnLog('Game saved in ' + T.ElapsedTime.ToString + 's.');
  LastSaveTimer := Timer;
end;

procedure LoadGame;
var
  Element: TDOMElement;
begin
  Element := UserConfig.PathElement('save_game', true);
  DungeonMap := TMap.Load(Element.ChildElement('map', true));
  Player := TPlayer.Load(Element.ChildElement('player', true));
  Player.LookAround; // We can't call it inside Load because it relies on Player variable
  LastSaveTimer := Timer;
end;

function SaveGameExists: Boolean;
var
  Element: TDOMElement;
begin
  Element := UserConfig.PathElement('save_game', false);
  Result := (Element <> nil) and (Element.FirstChild <> nil) and (UserConfig.GetValue('savegame_version', -1) = SaveGameVersion);
end;

procedure DeleteSaveGame;
begin
  UserConfig.DeletePath('save_game');
  UserConfig.Save;
end;

function SecondsSinceLastSave: Single;
begin
  Result := LastSaveTimer.ElapsedTime;
end;

end.

