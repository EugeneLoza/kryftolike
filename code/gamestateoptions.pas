{  Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStateOptions;

interface

uses
  Classes, SysUtils, CastleUiControls, CastleControls;

type
  TStateOptions = class(TCastleView)
  private
    ButtonSound0, ButtonSound25, ButtonSound50, ButtonSound75, ButtonSound100,
    ButtonMusic0, ButtonMusic25, ButtonMusic50, ButtonMusic75, ButtonMusic100,
    ButtonVibrationOn, ButtonVibrationOff, ButtonFullscreenOn, ButtonFullscreenOff,
    ButtonDeleteSaveGame:
      TCastleButton;
    procedure ClickCredits(Sender: TObject);
    procedure ClickDeleteSaveGame(Sender: TObject);
    procedure ClickMusic0(Sender: TObject);
    procedure ClickMusic100(Sender: TObject);
    procedure ClickMusic25(Sender: TObject);
    procedure ClickMusic50(Sender: TObject);
    procedure ClickMusic75(Sender: TObject);
    procedure ClickReturnToMainMenu(Sender: TObject);
    procedure ClickSound0(Sender: TObject);
    procedure ClickSound100(Sender: TObject);
    procedure ClickSound25(Sender: TObject);
    procedure ClickSound50(Sender: TObject);
    procedure ClickSound75(Sender: TObject);
    procedure ClickVibrationOn(Sender: TObject);
    procedure ClickVibrationOff(Sender: TObject);
    procedure ClickFullscreenOn(Sender: TObject);
    procedure ClickFullscreenOff(Sender: TObject);
    procedure UpdateUi;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  StateOptions: TStateOptions;

implementation
uses
  CastleComponentSerialize, CastleConfig, CastleSoundEngine,
  CastleApplicationProperties,
  GameStateCredits, GameStateMainMenu, GameSave, GameInitialize, GameSounds;

procedure TStateOptions.ClickCredits(Sender: TObject);
begin
  Sound('ui_click');
  Container.View := StateCredits;
end;

procedure TStateOptions.ClickDeleteSaveGame(Sender: TObject);
begin
  Sound('ui_click');
  ButtonDeleteSaveGame.Exists := false;
  DeleteSaveGame;
end;

procedure TStateOptions.ClickReturnToMainMenu(Sender: TObject);
begin
  Sound('ui_click');
  Container.View := StateMainMenu;
end;

procedure TStateOptions.ClickSound0(Sender: TObject);
begin
  SoundEngine.Volume := 0;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickSound25(Sender: TObject);
begin
  SoundEngine.Volume := 0.25;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickSound50(Sender: TObject);
begin
  SoundEngine.Volume := 0.5;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickSound75(Sender: TObject);
begin
  SoundEngine.Volume := 0.75;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickSound100(Sender: TObject);
begin
  SoundEngine.Volume := 1.0;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickMusic0(Sender: TObject);
begin
  SoundEngine.LoopingChannel[0].Volume := 0;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickMusic25(Sender: TObject);
begin
  SoundEngine.LoopingChannel[0].Volume := 0.25;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickMusic50(Sender: TObject);
begin
  SoundEngine.LoopingChannel[0].Volume := 0.5;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickMusic75(Sender: TObject);
begin
  SoundEngine.LoopingChannel[0].Volume := 0.75;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickMusic100(Sender: TObject);
begin
  SoundEngine.LoopingChannel[0].Volume := 1.0;
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickVibrationOn(Sender: TObject);
begin
  UserConfig.SetValue('vibration', true);
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickVibrationOff(Sender: TObject);
begin
  UserConfig.SetValue('vibration', false);
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickFullscreenOn(Sender: TObject);
begin
  SetFullScreen(true);
  UserConfig.SetValue('fullscreen', true);
  Sound('ui_click');
  UpdateUi;
end;

procedure TStateOptions.ClickFullscreenOff(Sender: TObject);
begin
  SetFullScreen(false);
  UserConfig.SetValue('fullscreen', false);
  Sound('ui_click');
  UpdateUi;
end;

constructor TStateOptions.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/options.castle-user-interface';
  DesignPreload := false;
end;

procedure TStateOptions.Start;
var
  VersionLabel: TCastleLabel;
begin
  inherited;

  (DesignedComponent('ButtonCredits') as TCastleButton).OnClick := @ClickCredits;
  (DesignedComponent('ButtonReturnToMainMenu') as TCastleButton).OnClick := @ClickReturnToMainMenu;

  ButtonSound0 := DesignedComponent('ButtonSound0') as TCastleButton;
  ButtonSound25 := DesignedComponent('ButtonSound25') as TCastleButton;
  ButtonSound50 := DesignedComponent('ButtonSound50') as TCastleButton;
  ButtonSound75 := DesignedComponent('ButtonSound75') as TCastleButton;
  ButtonSound100 := DesignedComponent('ButtonSound100') as TCastleButton;
  ButtonMusic0 := DesignedComponent('ButtonMusic0') as TCastleButton;
  ButtonMusic25 := DesignedComponent('ButtonMusic25') as TCastleButton;
  ButtonMusic50 := DesignedComponent('ButtonMusic50') as TCastleButton;
  ButtonMusic75 := DesignedComponent('ButtonMusic75') as TCastleButton;
  ButtonMusic100 := DesignedComponent('ButtonMusic100') as TCastleButton;

  ButtonSound0.OnClick := @ClickSound0;
  ButtonSound25.OnClick := @ClickSound25;
  ButtonSound50.OnClick := @ClickSound50;
  ButtonSound75.OnClick := @ClickSound75;
  ButtonSound100.OnClick := @ClickSound100;
  ButtonMusic0.OnClick := @ClickMusic0;
  ButtonMusic25.OnClick := @ClickMusic25;
  ButtonMusic50.OnClick := @ClickMusic50;
  ButtonMusic75.OnClick := @ClickMusic75;
  ButtonMusic100.OnClick := @ClickMusic100;

  ButtonVibrationOn := DesignedComponent('ButtonVibrationOn') as TCastleButton;
  ButtonVibrationOff := DesignedComponent('ButtonVibrationOff') as TCastleButton;
  ButtonVibrationOn.OnClick := @ClickVibrationOn;
  ButtonVibrationOff.OnClick := @ClickVibrationOff;

  ButtonFullscreenOn := DesignedComponent('ButtonFullscreenOn') as TCastleButton;
  ButtonFullscreenOff := DesignedComponent('ButtonFullscreenOff') as TCastleButton;
  ButtonFullscreenOn.OnClick := @ClickFullscreenOn;
  ButtonFullscreenOff.OnClick := @ClickFullscreenOff;

  (DesignedComponent('VibrationGroup') as TCastleUserInterface).Exists := {$ifdef ANDROID}true{$else}false{$endif};
  (DesignedComponent('FullscreenGroup') as TCastleUserInterface).Exists := {$ifdef ANDROID}false{$else}true{$endif};

  ButtonDeleteSaveGame := DesignedComponent('ButtonDeleteSaveGame') as TCastleButton;
  ButtonDeleteSaveGame.OnClick := @ClickDeleteSaveGame;
  ButtonDeleteSaveGame.Exists := SaveGameExists;

  VersionLabel := DesignedComponent('VersionLabel') as TCastleLabel;
  VersionLabel.Caption := 'Version: ' + ApplicationProperties.Version;

  UpdateUi;
end;

procedure TStateOptions.UpdateUi;
var
  CurrentVolume, CurrentMusicVolume: Integer;

  function SoundVolumeFontSize(const AVolume: Integer): Integer;
  begin
    if Trunc(5 * AVolume / 100) = CurrentVolume then
      Result := 50
    else
      Result := 25;
  end;

  function MusicVolumeFontSize(const AVolume: Integer): Integer;
  begin
    if Trunc(5 * AVolume / 100) = CurrentMusicVolume then
      Result := 50
    else
      Result := 25;
  end;

begin
  UserConfig.SetFloat('volume', SoundEngine.Volume);
  UserConfig.SetFloat('music', SoundEngine.LoopingChannel[0].Volume);
  CurrentVolume := Trunc(SoundEngine.Volume * 5);
  CurrentMusicVolume := Trunc(SoundEngine.LoopingChannel[0].Volume * 5);

  ButtonSound0.FontSize := SoundVolumeFontSize(0);
  ButtonSound25.FontSize := SoundVolumeFontSize(25);
  ButtonSound50.FontSize := SoundVolumeFontSize(50);
  ButtonSound75.FontSize := SoundVolumeFontSize(75);
  ButtonSound100.FontSize := SoundVolumeFontSize(100);
  ButtonMusic0.FontSize := MusicVolumeFontSize(0);
  ButtonMusic25.FontSize := MusicVolumeFontSize(25);
  ButtonMusic50.FontSize := MusicVolumeFontSize(50);
  ButtonMusic75.FontSize := MusicVolumeFontSize(75);
  ButtonMusic100.FontSize := MusicVolumeFontSize(100);

  if UserConfig.GetValue('vibration', true) then
  begin
    ButtonVibrationOn.FontSize := 50;
    ButtonVibrationOff.FontSize := 25;
  end else
  begin
    ButtonVibrationOn.FontSize := 25;
    ButtonVibrationOff.FontSize := 50;
  end;

  if UserConfig.GetValue('fullscreen', false) then
  begin
    ButtonFullscreenOn.FontSize := 50;
    ButtonFullscreenOff.FontSize := 25;
  end else
  begin
    ButtonFullscreenOn.FontSize := 25;
    ButtonFullscreenOff.FontSize := 50;
  end;
end;

end.

