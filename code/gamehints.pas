{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameHints;

{$mode objfpc}{$H+}

interface

uses
  GameContext, DOM;

type
  TGameHint = class(TObject)
  public
    Context: TContext;
    Text: String;
    procedure Validate;
    procedure Read(const Element: TDOMElement);
    function DebugString: String;
    constructor Create;
    destructor Destroy; override;
  end;

procedure LoadHints;
function GetRandomHint(const AContext: TContext): String;

implementation
uses
  SysUtils, Generics.Collections,
  CastleXMLUtils, CastleLog, CastleStringUtils,
  Global;

type
  THintList = specialize TObjectList<TGameHint>;

var
  HintList: THintList;
  AvailableHints: THintList; //reusable

function GetRandomHint(const AContext: TContext): String;
var
  Hint: TGameHint;
begin
  AvailableHints.Clear;
  for Hint in HintList do
    if Hint.Context.Convolution(AContext) then
      AvailableHints.Add(Hint);
  if AvailableHints.Count = 0 then
    raise Exception.Create('No hints that corresponds to the requested context!');
  Result := AvailableHints[Xorshift.Random(AvailableHints.Count)].Text;
end;

procedure LoadHints;
var
  Hint: TGameHint;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  HintList := THintList.Create(true);

  Doc := URLReadXML('castle-data:/hints.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('hint');
    try
      while Iterator.GetNext do
      begin
        Hint := TGameHint.Create;
        Hint.Read(Iterator.Current);
        Hint.Validate;
        //WriteLnLog(Hint.DebugString);
        HintList.Add(Hint);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
  WritelnLog('Hints/jokes loaded, total: %d', [HintList.Count]);
  AvailableHints := THintList.Create(false);
  AvailableHints.Capacity := HintList.Count;
end;

procedure TGameHint.Read(const Element: TDOMElement);
begin
  Text := Element.AttributeString('text');
  Context.ALLOW.AssignList(CreateTokens(Element.AttributeStringDef('allow', ''), [',']));
  Context.DENY.AssignList(CreateTokens(Element.AttributeStringDef('deny', ''), [',']));
  Context.DEMAND.AssignList(CreateTokens(Element.AttributeStringDef('demand', ''), [',']));
end;

function TGameHint.DebugString: String;
begin
  Result := '"' + Text + '" / ' + Context.DebugString;
end;

procedure TGameHint.Validate;
begin
  Context.Validate;
end;
constructor TGameHint.Create;
begin
  Context := TContext.Create;
end;
destructor TGameHint.Destroy;
begin
  Context.Free;
  inherited;
end;

finalization
  HintList.Free;
  AvailableHints.Free;
end.

