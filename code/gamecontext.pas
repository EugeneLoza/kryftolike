{ Copyright (C) 2020-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameContext;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  CastleStringUtils;

type
  TStringListHelper = class helper for TCastleStringList
  public
    function Contains(const AString: String): Boolean;
    procedure AssignList(const AList: TStrings);
  end;

type
  TContext = class(TObject)
  public
    Allow, Demand, Deny: TCastleStringList;
    function Convolution(const AContext: TContext): Boolean;
    procedure Validate;
    function DebugString: String;
    constructor Create;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils;

function TStringListHelper.Contains(const AString: String): Boolean;
var
  S: String;
begin
  for S in Self do
    if S = AString then
      Exit(true);
  Result := false;
end;

procedure TStringListHelper.AssignList(const AList: TStrings);
begin
  AddRange(AList);
  AList.Free;
end;

function TContext.Convolution(const AContext: TContext): Boolean;
var
  S: String;
begin
  for S in Self.Demand do
    if not AContext.Allow.Contains(S) and not AContext.Demand.Contains(S) then
      Exit(false);
  for S in AContext.Demand do
    if not Self.Allow.Contains(S) and not Self.Demand.Contains(S) then
      Exit(false);
  for S in Self.Deny do
    if AContext.Allow.Contains(S) or AContext.Demand.Contains(S) then
      Exit(false);
  for S in AContext.Deny do
    if Self.Allow.Contains(S) or Self.Demand.Contains(S) then
      Exit(false);
  Result := true;
end;

procedure TContext.Validate;
var
  S: String;
begin
  for S in Deny do
    if Demand.Contains(S) or Allow.Contains(S) then
      raise Exception.Create('DENY entry contains "' + S + '" but the context itself ALLOWs or DEMANDs it');
end;

function TContext.DebugString: String;
var
  S: String;
begin
  Result := 'ALLOW=[';
  for S in ALLOW do
    Result += S + ',';
  Result += '];DEMAND=[';
  for S in DEMAND do
    Result += S + ',';
  Result += '];DENY=[';
  for S in DENY do
    Result += S + ',';
  Result += ']';
end;

constructor TContext.Create;
begin
  Allow := TCastleStringList.Create;
  Demand := TCastleStringList.Create;
  Deny := TCastleStringList.Create;
end;
destructor TContext.Destroy;
begin
  Allow.Free;
  Demand.Free;
  Deny.Free;
  inherited;
end;

end.

