{  Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStateAchievements;

interface

uses
  Classes, SysUtils, CastleUiControls, CastleControls, CastleKeysMouse;

resourcestring
  HighScoreCaption = 'High Score : ';

type
  TStateAchievements = class(TCastleView)
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  StateAchievements: TStateAchievements;

implementation
uses
  CastleComponentSerialize, CastleConfig,
  GameAchievements, GameStateMainMenu, GameSounds;

constructor TStateAchievements.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/achievements.castle-user-interface';
  DesignPreload := false;
end;

procedure TStateAchievements.Start;

  procedure ShowAchievement(const AName: String);
  begin
    (DesignedComponent(AName) as TCastleDesign).Exists := HasAchievement(AName);
  end;

begin
  inherited;

  ShowAchievement('die_1');
  ShowAchievement('die_12');
  ShowAchievement('die_32');
  ShowAchievement('die_64');
  ShowAchievement('die_teleport');
  ShowAchievement('die_teleport_3');
  ShowAchievement('monsters_3');
  ShowAchievement('monsters_5');
  ShowAchievement('monsters_7');
  ShowAchievement('rebel');
  ShowAchievement('score_6k');
  ShowAchievement('score_12k');
  ShowAchievement('score_20k');
  ShowAchievement('score_35k');
  ShowAchievement('score_50k');
  ShowAchievement('teleport_1');
  ShowAchievement('teleport_3');
  ShowAchievement('teleport_5');
  ShowAchievement('travel_2k');
  ShowAchievement('travel_7k');
  ShowAchievement('travel_15k');

  (DesignedComponent('LabelHighScoreShadow') as TCastleLabel).Caption := HighScoreCaption + UserConfig.GetValue('high_score', 0).ToString;
  (DesignedComponent('LabelHighScore') as TCastleLabel).Caption := HighScoreCaption + UserConfig.GetValue('high_score', 0).ToString;
end;

function TStateAchievements.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  Sound('ui_click');
  Container.View := StateMainMenu;
end;

end.

