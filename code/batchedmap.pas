{ MIT-License

  Copyright (c) 2021-2024 Yevhen Loza

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
}

unit BatchedMap;

{$mode objfpc}{$H+}

interface

uses
  CastleUiControls, CastleRectangles, CastleKeysMouse,
  Global;

const
  TileSize = 32;

type
  TMapClick = procedure (const ClickV: TCoordinateVector) of object;

type
  TBatchedMap = class(TCastleUserInterface)
  strict private
    ShiftX, ShiftY: Integer;
    ScreenToGlobalX, ScreenToGlobalY: TSignedCoordinate;
    ScreenRects, ImageRects: array of TFloatRectangle;
  public
    CenterX, CenterY: TCoordinate;
    Zoom: Single;
    OnMapClick: TMapClick;
    procedure Render; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

procedure LoadTileset;

implementation
uses
  SysUtils, Math,
  CastleGlImages, CastleVectors,
  GameMap, GamePlayer, GameMapObject, GameMapChunk;

var
  FTileset: TDrawableImage;

procedure TBatchedMap.Render;
const
  IndexesPerQuad = 6;
  MaxRenderQuads = 65535 div IndexesPerQuad - 2;
var
  Count: Integer;
  SX, SY: Integer;
  RX, RY: Integer;
  ViewX, ViewY: Integer;

  procedure RenderMap;
  var
    X, Y: Integer;
    MapX, MapY: TCoordinate;
  begin
    for X := 0 to ViewX - 1 do
      for Y := 0 to ViewY - 1 do
      begin
        MapX := X + ScreenToGlobalX;
        MapY := Y + ScreenToGlobalY;
        if DungeonMap.GetVisible(MapX, MapY) then
        begin
          SX := X * TileSize;
          SY := Y * TileSize;
          ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
          RX := DungeonMap.GetTile(MapX, MapY) * Tilesize;
          RY := DungeonMap.GetBiome(MapX, MapY) * Tilesize;
          ImageRects[Count] := FloatRectangle(RX, RY, TileSize, TileSize);
          Inc(Count);
        end;
      end;
  end;

  procedure RenderFogOfWar;
  var
    X, Y: Integer;
    MapX, MapY: TCoordinate;
  begin
    for X := 0 to ViewX - 1 do
      for Y := 0 to ViewY - 1 do
      begin
        MapX := X + ScreenToGlobalX;
        MapY := Y + ScreenToGlobalY;
        if DungeonMap.GetVisible(MapX, MapY) and not Player.CanSeeNow(MapX, MapY) then
        begin
          SX := X * TileSize;
          SY := Y * TileSize;
          ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
          ImageRects[Count] := FloatRectangle(31 * Tilesize, (2 + DungeonMap.GetTile(MapX, MapY)) * Tilesize, TileSize, TileSize);
          Inc(Count);
        end;
      end;
   end;

   procedure RenderUnits;
   var
     DX, DY: Integer;
     MapObject: TMapObject;
   begin
     // Render Sigils
     for DX := -1 to 1 do
       for DY := -1 to 1 do
         for MapObject in DungeonMap.GetChunkAroundPlayer(DX, DY).Sigils do
           if DungeonMap.GetVisible(MapObject.X, MapObject.Y) then
           begin
             SX := (MapObject.X - ScreenToGlobalX) * TileSize;
             SY := (MapObject.Y - ScreenToGlobalY) * TileSize;
             ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
             ImageRects[Count] := FloatRectangle(30 * Tilesize, (13 + MapObject.Tile) * Tilesize, TileSize, TileSize);
             Inc(Count);
           end;

     // Render treasure
     for DX := -1 to 1 do
       for DY := -1 to 1 do
         for MapObject in DungeonMap.GetChunkAroundPlayer(DX, DY).Treasure do
           if DungeonMap.GetVisible(MapObject.X, MapObject.Y) then
           begin
             SX := (MapObject.X - ScreenToGlobalX) * TileSize;
             SY := (MapObject.Y - ScreenToGlobalY) * TileSize;
             ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
             ImageRects[Count] := FloatRectangle(30 * Tilesize, (7 + MapObject.Tile) * Tilesize, TileSize, TileSize);
             Inc(Count);
           end;

     // Render enemies
     for DX := -1 to 1 do
       for DY := -1 to 1 do
         for MapObject in DungeonMap.GetChunkAroundPlayer(DX, DY).Enemies do
           if Player.CanSeeNow(MapObject.X, MapObject.Y) then
           begin
             SX := (MapObject.X - ScreenToGlobalX) * TileSize;
             SY := (MapObject.Y - ScreenToGlobalY) * TileSize;
             ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
             ImageRects[Count] := FloatRectangle(30 * Tilesize, (31 - MapObject.Tile) * Tilesize, TileSize, TileSize);
             Inc(Count);
           end;

     // Render player alert point
     if Player.IsAlive and (Player.AlertTimeout > 0) then
     begin
       SX := (Player.AlertPoint.X - ScreenToGlobalX) * TileSize;
       SY := (Player.AlertPoint.Y - ScreenToGlobalY) * TileSize;
       ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
       ImageRects[Count] := FloatRectangle(31 * TileSize, 1 * TileSize, TileSize, TileSize);
       Inc(Count)
     end;

     // Render player;
     SX := (Player.X - ScreenToGlobalX) * TileSize;
     SY := (Player.Y - ScreenToGlobalY) * TileSize;
     ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
     if Player.IsAlive then
     begin
       ImageRects[Count] := FloatRectangle(30 * TileSize, (Player.Tile + 1) * TileSize, TileSize, TileSize);
     end else
       ImageRects[Count] := FloatRectangle(30 * TileSize, 0, TileSize, TileSize);
     Inc(Count);
   end;

  procedure RenderZoomOutOverlay;
  var
    X, Y: Integer;
    MapX, MapY: TCoordinate;
  begin
    for X := 0 to ViewX - 1 do
      for Y := 0 to ViewY - 1 do
      begin
        MapX := X + ScreenToGlobalX;
        MapY := Y + ScreenToGlobalY;
        if DungeonMap.GetVisible(MapX, MapY) then
        begin
          SX := X * TileSize;
          SY := Y * TileSize;
          ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
          if DungeonMap.CanPass(MapX, MapY) then
            ImageRects[Count] := FloatRectangle(31 * Tilesize, 0, 1, 1)
          else
            ImageRects[Count] := FloatRectangle(31 * Tilesize + 1, 0, 1, 1);
          Inc(Count);
        end;
      end;
   end;

   procedure RenderZoomOutUnits;
   var
     DX, DY: Integer;
     MapObject: TMapObject;
     Chunk: TChunk;
   begin
     // Render Sigils
     for DX := -1 to 1 do
       for DY := -1 to 1 do
         for MapObject in DungeonMap.GetChunkAroundPlayer(DX, DY).Sigils do
           if DungeonMap.GetVisible(MapObject.X, MapObject.Y) then
           begin
             SX := (MapObject.X - ScreenToGlobalX) * TileSize;
             SY := (MapObject.Y - ScreenToGlobalY) * TileSize;
             ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
             ImageRects[Count] := FloatRectangle(31 * Tilesize + 5, 0, 1, 1);
             Inc(Count);
           end;

     // Render Treasure
     for DX := -10 to 10 do
       for DY := -10 to 10 do
       begin
         Chunk := DungeonMap.GetChunkAroundPlayer(DX, DY);
         if Chunk <> nil then
           for MapObject in Chunk.Treasure do
             if DungeonMap.GetVisible(MapObject.X, MapObject.Y) then
             begin
               SX := (MapObject.X - ScreenToGlobalX) * TileSize;
               SY := (MapObject.Y - ScreenToGlobalY) * TileSize;
               ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
               ImageRects[Count] := FloatRectangle(31 * Tilesize + 4, 0, 1, 1);
               Inc(Count);
             end;
       end;

     // Render enemies
     for DX := -1 to 1 do
       for DY := -1 to 1 do
         for MapObject in DungeonMap.GetChunkAroundPlayer(DX, DY).Enemies do
           if Player.CanSeeNow(MapObject.X, MapObject.Y) then
           begin
             SX := (MapObject.X - ScreenToGlobalX) * TileSize;
             SY := (MapObject.Y - ScreenToGlobalY) * TileSize;
             ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
             ImageRects[Count] := FloatRectangle(31 * Tilesize + 3, 0, 1, 1);
             Inc(Count);
           end;
     // Render player;
     SX := (Player.X - ScreenToGlobalX) * TileSize;
     SY := (Player.Y - ScreenToGlobalY) * TileSize;
     ScreenRects[Count] := FloatRectangle(SX * Zoom - ShiftX, SY * Zoom - ShiftY, TileSize * Zoom, TileSize * Zoom);
     ImageRects[Count] := FloatRectangle(31 * Tilesize + 2, 0, 1, 1);
     Inc(Count);
   end;

begin
  inherited; // Parent is empty
  // Calculate the real screen size
  ViewX := 2 * Ceil(RenderRect.Width / TileSize / Zoom / 2) + 1;
  ViewY := 2 * Ceil(RenderRect.Height / TileSize / Zoom / 2) + 1;
  ShiftX := Round(ViewX * TileSize * Zoom - RenderRect.Width) div 2 - Round(RenderRect.Left); // I have absolutely no idea why "minus", but it works
  ShiftY := Round(ViewY * TileSize * Zoom - RenderRect.Height) div 2 - Round(RenderRect.Bottom);
  ScreenToGlobalX := CenterX - ViewX div 2;
  ScreenToGlobalY := CenterY - ViewY div 2;
  if Length(ScreenRects) < 4 * ViewX * ViewY then
  begin
    SetLength(ScreenRects, 4 * ViewX * ViewY);
    SetLength(ImageRects, 4 * ViewX * ViewY);
  end;
  Count := 0;
  FTileset.Color := Vector4(1, 1, 1, 1);
  if (TileSize * Zoom > 4) then
  begin
    RenderMap;
    RenderFogOfWar;
    RenderUnits;
  end else
  begin
    RenderZoomOutOverlay;
    RenderZoomOutUnits;
  end;

  //copy from castleconf.inc
  {$ifdef ANDROID} {$define OpenGLES} {$endif}
  {$ifdef CASTLE_IOS} {$define OpenGLES} {$endif}
  {$ifdef OpenGLES} {$define GLIndexesShort} {$endif}

  {$ifdef GLIndexesShort}
  while Count >= MaxRenderQuads do
  begin
    FTileset.Draw(@ScreenRects[Count - MaxRenderQuads], @ImageRects[Count - MaxRenderQuads], MaxRenderQuads);
    Count -= MaxRenderQuads;
  end;
  {$endif}
  FTileset.Draw(@ScreenRects[0], @ImageRects[0], Count);

  if (TileSize * Zoom > 4) and (Zoom < 1) then
  begin
    // We cannot render the image with two different Color-s in one batch
    // If we want a semi-transparent overlay, we render it in a new pass
    Count := 0;
    FTileset.Color := Vector4(1, 1, 1, Sqr(1 - Zoom));
    RenderZoomOutOverlay;
    RenderZoomOutUnits;
    {$ifdef GLIndexesShort}
    while Count >= MaxRenderQuads do
    begin
      FTileset.Draw(@ScreenRects[Count - MaxRenderQuads], @ImageRects[Count - MaxRenderQuads], MaxRenderQuads);
      Count -= MaxRenderQuads;
    end;
    {$endif}
    FTileset.Draw(@ScreenRects[0], @ImageRects[0], Count);
  end;
end;

function TBatchedMap.Press(const Event: TInputPressRelease): Boolean;
var
  ClickX, ClickY: TCoordinate;
begin
  Result := inherited;
  if Result then
    Exit;

  if (Event.EventType = itMouseButton) and (Assigned(OnMapClick)) then
  begin
    { We count that ScreenToGlobalX/ShiftX are cached }
    ClickX := Trunc(Double(Event.Position.X + ShiftX) / Zoom / TileSize) + ScreenToGlobalX;
    ClickY := Trunc(Double(Event.Position.Y + ShiftY) / Zoom / TileSize) + ScreenToGlobalY;
    OnMapClick(CoordinateVector(ClickX, ClickY));
  end;
end;

procedure LoadTileset;
begin
  FTileset := TDrawableImage.Create('castle-data:/tileset/tileset.png', false);
end;

finalization
  FTileset.Free;
end.

