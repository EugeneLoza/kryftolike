{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{}
unit GameMapChunk;

{$mode objfpc}{$H+}

interface
uses
  Generics.Collections, DOM,
  Global, GameMapObject;

const
  ChunkSize = 32;

const
  Undefined = 254;
  AnchorPoint = 255;

type
  TAnchorPointsList = specialize TList<TCoordinateVector>;

type
  TChunk = class(TObject)
  public
    Tile: array [0..ChunkSize - 1, 0..ChunkSize - 1] of Byte;
    Biome: array [0..ChunkSize - 1, 0..ChunkSize - 1] of Byte;
    Visible: array [0..ChunkSize - 1, 0..ChunkSize - 1] of Boolean;
    Treasure: TMapObjectList;
    Sigils: TSigilsList;
    Enemies: TEnemyList;
    BiomeCenterX: TCoordinate;
    BiomeCenterY: TCoordinate;
    BiomeRangeMultiplier: Single;
    BiomeCenterSeed: Byte;
    AnchorPoints: TAnchorPointsList;
    procedure Save(const Element: TDOMElement; const ChunkCoordinates: TCoordinateVector);
    class function Load(const Element: TDOMElement): TChunk;
    constructor Create; //override;
    destructor Destroy; override;
  end;

type
  TChunksDictionary = specialize TObjectDictionary<TCoordinateVector, TChunk>;

implementation
uses
  SysUtils, Base64,
  CastleXmlUtils;

procedure TChunk.Save(const Element: TDOMElement; const ChunkCoordinates: TCoordinateVector);

  function BoolToChar(const ABool: Boolean): Char;
  begin
    if ABool then
      Result := '1'
    else
      Result := '0';
  end;

var
  IX, IY: Integer;
  TilesString, BiomesString, VisibleString: RawByteString;
  ThisElement, AnchorPointElement: TDOMElement;
  T: TMapObject;
  E: TEnemy;
  V: TCoordinateVector;
begin
  ThisElement := Element.CreateChild('chunk');
  ThisElement.AttributeSet('X', ChunkCoordinates.X);
  ThisElement.AttributeSet('Y', ChunkCoordinates.Y);

  TilesString := '';
  BiomesString := '';
  VisibleString := '';
  for IX := 0 to ChunkSize - 1 do
    for IY := 0 to ChunkSize - 1 do
    begin
      TilesString += Char(Tile[IX, IY]);
      BiomesString += Char(Biome[IX, IY]);
      VisibleString += BoolToChar(Visible[IX, IY]);
    end;
  ThisElement.AttributeSet('tile', EncodeStringBase64(TilesString));
  ThisElement.AttributeSet('biome', EncodeStringBase64(BiomesString));
  ThisElement.AttributeSet('vis', EncodeStringBase64(VisibleString));
  for T in Treasure do
    T.Save(ThisElement);
  for T in Sigils do
    T.Save(ThisElement);
  for E in Enemies do
    E.Save(ThisElement);

  ThisElement.AttributeSet('BiomeCenterX', BiomeCenterX);
  ThisElement.AttributeSet('BiomeCenterY', BiomeCenterY);
  ThisElement.AttributeSet('BiomeRangeMultiplier', BiomeRangeMultiplier);
  ThisElement.AttributeSet('BiomeCenterSeed', Integer(BiomeCenterSeed));

  for V in AnchorPoints do
  begin
    AnchorPointElement := ThisElement.CreateChild('anchor');
    AnchorPointElement.AttributeSet('x', V.X);
    AnchorPointElement.AttributeSet('y', V.Y);
  end;
end;

class function TChunk.Load(const Element: TDOMElement): TChunk;

  function CharToBool(const AChar: Char): Boolean;
  begin
    case AChar of
      '1': Result := true;
      '0': Result := false;
      else
        raise Exception.Create('Unexpected char in visible: ' + AChar);
    end;
  end;

var
  IX, IY, K: Integer;
  TilesString, BiomesString, VisibleString: RawByteString;
  I: TXMLElementIterator;
begin
  Result := TChunk.Create;
  TilesString := DecodeStringBase64(Element.AttributeString('tile'));
  BiomesString := DecodeStringBase64(Element.AttributeString('biome'));
  VisibleString := DecodeStringBase64(Element.AttributeString('vis'));
  K := 0;
  for IX := 0 to ChunkSize - 1 do
    for IY := 0 to ChunkSize - 1 do
    begin
      Inc(K);
      Result.Tile[IX, IY] := Ord(TilesString[K]);
      Result.Biome[IX, IY] := Ord(BiomesString[K]);
      Result.Visible[IX, IY] := CharToBool(VisibleString[K]);
    end;

  I := Element.ChildrenIterator('treasure');
  try
    while I.GetNext do
      Result.Treasure.Add(TMapObject.Load(I.Current));
  finally
    FreeAndNil(I)
  end;
  I := Element.ChildrenIterator('sigil');
  try
    while I.GetNext do
      Result.Sigils.Add(TSigil.Load(I.Current));
  finally
    FreeAndNil(I)
  end;
  I := Element.ChildrenIterator('enemy');
  try
    while I.GetNext do
      Result.Enemies.Add(TEnemy.Load(I.Current));
  finally
    FreeAndNil(I)
  end;

  Result.BiomeCenterX := Element.AttributeInt64('BiomeCenterX');
  Result.BiomeCenterY := Element.AttributeInt64('BiomeCenterY');
  Result.BiomeRangeMultiplier := Element.AttributeSingle('BiomeRangeMultiplier');
  Result.BiomeCenterSeed := Byte(Element.AttributeInteger('BiomeCenterSeed'));

  I := Element.ChildrenIterator('anchor');
  try
    while I.GetNext do
      Result.AnchorPoints.Add(CoordinateVector(I.Current.AttributeInt64('x'), I.Current.AttributeInt64('y')));
  finally
    FreeAndNil(I)
  end;
end;

constructor TChunk.Create;
var
  X, Y: Integer;
begin
  //inherited
  BiomeCenterX := Undefined;
  BiomeCenterY := Undefined;
  BiomeCenterSeed := Undefined;
  for X := 0 to ChunkSize - 1 do
    for Y := 0 to ChunkSize - 1 do
    begin
      Tile[X, Y] := Undefined;
      Biome[X, Y] := Undefined;
      Visible[X, Y] := false;
    end;
  AnchorPoints := TAnchorPointsList.Create;
  Treasure := TMapObjectList.Create(true);
  Sigils := TSigilsList.Create(true);
  Enemies := TEnemyList.Create(true);
end;

destructor TChunk.Destroy;
begin
  FreeAndNil(AnchorPoints);
  FreeAndNil(Treasure);
  FreeAndNil(Sigils);
  FreeAndNil(Enemies);
  inherited;
end;

end.

