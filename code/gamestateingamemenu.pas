{  Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStateInGameMenu;

interface

uses
  Classes, SysUtils, CastleUiControls, CastleControls;

type
  TStateInGameMenu = class(TCastleView)
  private
    procedure ClickResume(Sender: TObject);
    procedure ClickExitNoSave(Sender: TObject);
    procedure ClickExitAndSave(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  StateInGameMenu: TStateInGameMenu;

implementation
uses
  CastleComponentSerialize,
  GameStateMainMenu, GameSave, GamePlayer;

constructor TStateInGameMenu.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/in_game_menu.castle-user-interface';
  DesignPreload := true;
end;

procedure TStateInGameMenu.Start;
var
  ButtonReturnToGame, ButtonExitToMenu, ButtonExitAndSave: TCastleButton;
begin
  inherited;

  InterceptInput := true;

  ButtonReturnToGame := DesignedComponent('ButtonReturnToGame') as TCastleButton;
  ButtonExitToMenu := DesignedComponent('ButtonExitToMenu') as TCastleButton;
  ButtonExitAndSave := DesignedComponent('ButtonExitAndSave') as TCastleButton;
  ButtonReturnToGame.OnClick := @ClickResume;
  ButtonReturnToGame.Exists := Player.isAlive;
  ButtonExitToMenu.OnClick := @ClickExitNoSave;
  ButtonExitToMenu.Exists := not Player.isAlive;
  ButtonExitAndSave.OnClick := @ClickExitAndSave;
  ButtonExitAndSave.Exists := Player.isAlive;
end;

procedure TStateInGameMenu.ClickResume(Sender: TObject);
begin
  Container.PopView(Self);
end;

procedure TStateInGameMenu.ClickExitAndSave(Sender: TObject);
begin
  SaveGame;
  Container.View := StateMainMenu;
end;

procedure TStateInGameMenu.ClickExitNoSave(Sender: TObject);
begin
  //DeleteSaveGame; // Player.Die already deletes the game
  Container.View := StateMainMenu;
end;

end.
