{  Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameViewHowToPlay;

interface

uses
  Classes, SysUtils, CastleUiControls, CastleKeysMouse;

type
  TViewHowToPlay = class(TCastleView)
  private
    procedure ClickReturn(Sender: TObject);
  public
    procedure Start; override;
    constructor Create(AOwner: TComponent); override;
  end;

var
  ViewHowToPlay: TViewHowToPlay;

implementation
uses
  CastleComponentSerialize, CastleControls,
  GameStateMainMenu, GameSounds;

procedure TViewHowToPlay.ClickReturn(Sender: TObject);
begin
  Sound('ui_click');
  Container.View := StateMainMenu;
end;

procedure TViewHowToPlay.Start;
begin
  inherited Start;
  (DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ClickReturn;
end;

constructor TViewHowToPlay.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/howtoplay.castle-user-interface';
  DesignPreload := false;
end;

end.

