{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{}
unit GameMapObject;

{$mode objfpc}{$H+}

interface
uses
  Generics.Collections, DOM,
  Global;

const
  ChanceToSkipMove = 0.12;

type
  { An object on the map }
  TMapObject = class(TObject)
  protected
    function Signature: String; virtual;
  public
    { Current global coordinates of this object }
    X, Y: TCoordinate;
    { Sprite of this object }
    Tile: Byte;
    procedure Save(const Element: TDOMElement);
    class function Load(const Element: TDOMElement): TMapObject;
  end;

type
  TSigil = class(TMapObject)
  protected
    function Signature: String; override;
  public
    class function Load(const Element: TDOMElement): TSigil;
  end;

type
  TEnemy = class(TMapObject)
  protected
    function Signature: String; override;
  public
    { If the Enemy didn't move this turn (to avoid making two turns in case
      the Enemy moved from one chunk to another) }
    ReadyToMove: Boolean;
    function Move(const DX, DY: Integer): Boolean;
    class function Load(const Element: TDOMElement): TEnemy;
  end;

type
  TMapObjectList = specialize TObjectList<TMapObject>;
  TSigilsList = specialize TObjectList<TSigil>;
  TEnemyList = specialize TObjectList<TEnemy>;

implementation
uses
  CastleXmlUtils,
  GameMap, GameMapChunk;

function TEnemy.Move(const DX, DY: Integer): Boolean;
var
  V1, V2: TCoordinateVector;
begin
  if ReadyToMove and (Xorshift.Random < ChanceToSkipMove) then
  begin
    ReadyToMove := false;
    Exit(false);
  end;

  if DungeonMap.CanPassEnemy(X + DX, Y + DY) then
  begin
    V1 := CoordinateVector(X div ChunkSize, Y div ChunkSize);
    X += DX;
    Y += DY;
    V2 := CoordinateVector(X div ChunkSize, Y div ChunkSize);
    Result := true;
    ReadyToMove := false;
    if (V1.X <> V2.X) or (V1.Y <> V2.Y) then
    begin
      DungeonMap.Chunks[V1].Enemies.Extract(Self);
      DungeonMap.Chunks[V2].Enemies.Add(Self);
    end;
  end else
    Result := false;
end;

function TMapObject.Signature: String;
begin
  Exit('treasure');
end;

procedure TMapObject.Save(const Element: TDOMElement);
var
  ThisElement: TDOMElement;
begin
  ThisElement := Element.CreateChild(Signature);
  ThisElement.AttributeSet('x', X);
  ThisElement.AttributeSet('y', Y);
  ThisElement.AttributeSet('tile', Integer(Tile)); // Why typecast is needed to grab a proper overload?
end;

class function TMapObject.Load(const Element: TDOMElement): TMapObject;
begin
  Result := TMapObject.Create;
  Result.X := Element.AttributeInt64('x');
  Result.Y := Element.AttributeInt64('y');
  Result.Tile := Element.AttributeInteger('tile');
end;

function TSigil.Signature: String;
begin
  Exit('sigil');
end;

class function TSigil.Load(const Element: TDOMElement): TSigil;
begin
  Result := TSigil.Create;
  Result.X := Element.AttributeInt64('x');
  Result.Y := Element.AttributeInt64('y');
  Result.Tile := Byte(Element.AttributeInteger('tile'));
end;

function TEnemy.Signature: String;
begin
  Exit('enemy');
end;

class function TEnemy.Load(const Element: TDOMElement): TEnemy;
begin
  Result := TEnemy.Create;
  Result.X := Element.AttributeInt64('x');
  Result.Y := Element.AttributeInt64('y');
  Result.Tile := Byte(Element.AttributeInteger('tile'));
  Result.ReadyToMove := true;
end;

end.

