{  Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStateMainMenu;

interface

uses
  Classes, SysUtils, CastleUiControls, CastleControls;

type
  TStateMainMenu = class(TCastleView)
  strict private
    procedure ClickHowToPlay(Sender: TObject);
    procedure ClickLoad(Sender: TObject);
    procedure ClickStart(Sender: TObject);
    procedure ClickAchievements(Sender: TObject);
    procedure ClickOptions(Sender: TObject);
    procedure ClickQuit(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  StateMainMenu: TStateMainMenu;

implementation
uses
  CastleComponentSerialize,
  CastleWindow, CastleApplicationProperties,
  GameStateMain, GameStateAchievements, GameStateOptions, GameViewHowToPlay,
  GameSave, GameSounds;

constructor TStateMainMenu.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/mainmenu.castle-user-interface';
  DesignPreload := true;
end;

procedure TStateMainMenu.Start;
var
  ButtonLoadGame, ButtonStartGame, ButtonAchievements, ButtonOptions, ButtonQuit: TCastleButton;
begin
  inherited;

  ButtonLoadGame := DesignedComponent('ButtonLoadGame') as TCastleButton;
  ButtonStartGame := DesignedComponent('ButtonStartGame') as TCastleButton;
  ButtonAchievements := DesignedComponent('ButtonAchievements') as TCastleButton;
  ButtonOptions := DesignedComponent('ButtonOptions') as TCastleButton;
  ButtonQuit := DesignedComponent('ButtonQuit') as TCastleButton;
  (DesignedComponent('ButtonHowToPlay') as TCastleButton).OnClick := @ClickHowToPlay;
  ButtonLoadGame.OnClick := @ClickLoad;
  ButtonLoadGame.Exists := SaveGameExists;
  ButtonStartGame.OnClick := @ClickStart;
  ButtonStartGame.Exists := not ButtonLoadGame.Exists; // just because we don't have enough space on screen
  ButtonAchievements.OnClick := @ClickAchievements;
  ButtonOptions.OnClick := @ClickOptions;
  ButtonQuit.OnClick := @ClickQuit;
  ButtonQuit.Exists := ApplicationProperties.ShowUserInterfaceToQuit;

  Music('menu_music');
end;

procedure TStateMainMenu.ClickHowToPlay(Sender: TObject);
begin
  Sound('ui_click');
  Container.View := ViewHowToPlay;
end;

procedure TStateMainMenu.ClickLoad(Sender: TObject);
begin
  Sound('ui_click');
  StateMain.DoLoadGame := true;
  Container.View := StateMain;
end;

procedure TStateMainMenu.ClickStart(Sender: TObject);
begin
  Sound('ui_click');
  StateMain.DoLoadGame := false;
  Container.View := StateMain;
end;

procedure TStateMainMenu.ClickAchievements(Sender: TObject);
begin
  Sound('ui_click');
  Container.View := StateAchievements;
end;

procedure TStateMainMenu.ClickOptions(Sender: TObject);
begin
  Sound('ui_click');
  Container.View := StateOptions;
end;

procedure TStateMainMenu.ClickQuit(Sender: TObject);
begin
  Sound('ui_click');
  Application.MainWindow.Close;
end;

end.
