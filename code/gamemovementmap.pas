{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMovementMap;

interface

uses
  Classes, SysUtils,
  Global;

type
  TMovementDirection = Byte;

const
  { Movement direction aka enum, but 1-byte length }
  mdWall = TMovementDirection(0);
  mdUndefined = TMovementDirection(1);
  mdUp = TMovementDirection(2);
  mdRight = TMovementDirection(3);
  mdDown = TMovementDirection(4);
  mdLeft = TMovementDirection(5);
  mdZero = TMovementDirection(6);

type
  TMovementArray = packed array of TMovementDirection;

type
  TMovementMap = class(TObject)
  strict private
    MovementMap: TMovementArray;
    MovementMapForPoint: TCoordinateVector;
    function Index(const AX, AY: Integer): Integer; inline;
  public
    Size: Integer;
    procedure BuildMovementMap(const BuildForPoint: TCoordinateVector; const MaxMovementRange: Integer; const IgnoreVisible: Boolean; const FromPosition: TCoordinateVector; const ConsiderFromPosition: Boolean);
    function GetMovementMapSafe(const AX, AY: TCoordinate): TMovementDirection;
  end;

implementation
uses
  CastleLog,
  GameMap;

function TMovementMap.Index(const AX, AY: Integer): Integer; inline;
begin
  Result := (AX + Size) + (AY + Size) * Size * 2;
end;

procedure TMovementMap.BuildMovementMap(const BuildForPoint: TCoordinateVector; const MaxMovementRange: Integer; const IgnoreVisible: Boolean; const FromPosition: TCoordinateVector; const ConsiderFromPosition: Boolean);
var
  IX, IY: Integer;
  FPX, FPY: Integer;
  MinX, MaxX, MinY, MaxY: Integer;
  NoMoreMoves: Boolean;
  TotalMovementDistance: Integer;
  OldMovementMap: TMovementArray;

    function TryMovementWithDeltas(const DX, DY: Integer; const MovementDirection: TMovementDirection): Boolean;
    begin
      if OldMovementMap[Index(IX + DX, IY + DY)] > mdUndefined then
      begin
        MovementMap[Index(IX, IY)] := MovementDirection;
        NoMoreMoves := false;
        if (IX = MinX) and (MinX > -Size + 1) then
          Dec(MinX);
        if (IX = MaxX) and (MaxX < Size - 1) then
          Inc(MaxX);
        if (IY = MinY) and (MinY > -Size + 1) then
          Dec(MinY);
        if (IY = MaxY) and (MaxY < Size - 1) then
          Inc(MaxY);
        Result := true;
      end else
        Result := false;
    end;

begin
  if (MovementMapForPoint.X = BuildForPoint.X) and (MovementMapForPoint.Y = BuildForPoint.Y) then
    Exit;

  if ConsiderFromPosition then
  begin
    FPX := FromPosition.X - BuildForPoint.X;
    FPY := FromPosition.Y - BuildForPoint.Y;
    if (Abs(FPX) > Size) or (Abs(FPY) > Size) then
      Exit;
  end;

  MovementMapForPoint := BuildForPoint;

  MovementMap := nil;
  OldMovementMap := nil;
  SetLength(MovementMap, (Size + 1) * (Size + 1) * 4 + 1);
  SetLength(OldMovementMap, (Size + 1) * (Size + 1) * 4 + 1);
  FillByte(MovementMap[0], (Size + 1) * (Size + 1) * 4 + 1, mdUndefined);

  TotalMovementDistance := 0;
  MovementMap[Index(0, 0)] := mdZero;
  MinX := -1;
  MaxX := 1;
  MinY := -1;
  MaxY := 1;
  repeat
    Move(MovementMap[0], OldMovementMap[0], Length(MovementMap));
    NoMoreMoves := true;
    for IX := MinX to MaxX do
      for IY := MinY to MaxY do
        if (OldMovementMap[Index(IX, IY)] = mdUndefined) then
        begin
          if DungeonMap.CanPass(IX + BuildForPoint.X, IY + BuildForPoint.Y) and
            (IgnoreVisible or DungeonMap.GetVisible(IX + BuildForPoint.X, IY + BuildForPoint.Y)) then
          begin
            if not TryMovementWithDeltas(-1,  0, mdLeft) then
            if not TryMovementWithDeltas(+1,  0, mdRight) then
            if not TryMovementWithDeltas( 0, -1, mdDown) then
                   TryMovementWithDeltas( 0, +1, mdUp);
          end else
            MovementMap[Index(IX, IY)] := mdWall;
        end;
    Inc(TotalMovementDistance);
  until (TotalMovementDistance > MaxMovementRange) or NoMoreMoves or (ConsiderFromPosition and (MovementMap[Index(FPX, FPY)] <> mdUndefined));
end;

function TMovementMap.GetMovementMapSafe(const AX, AY: TCoordinate): TMovementDirection;
begin
  if (MovementMap <> nil) and
     (AX - MovementMapForPoint.X >= -Size) and
     (AX - MovementMapForPoint.X <=  Size) and
     (AY - MovementMapForPoint.Y >= -Size) and
     (AY - MovementMapForPoint.Y <=  Size) then
    Result := MovementMap[Index(AX - MovementMapForPoint.X, AY - MovementMapForPoint.Y)]
  else
    Result := mdUndefined;
end;

end.

