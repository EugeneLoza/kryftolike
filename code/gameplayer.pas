{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePlayer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DOM,
  CastleVectors,
  Global, GameContext, GameMovementMap;

//{$define debugteleport}

const
  VisibilityRange = 7;
  MinTeleportRange = 32;
  MaxTeleportRange = 64;
  SafeTeleportArea = VisibilityRange;
  EnemyAlertDuration = 21;
  TreasureAlertDuration = 7;
  EnemySurpriseSafetyDuration = 5;
  PostTeleoprtSafetyDuration = 12;

type
  TPlayer = class(TObject)
  private
    FCachedHighScore: Integer;
    Visibility: array [-VisibilityRange..VisibilityRange, -VisibilityRange..VisibilityRange] of Boolean;
    procedure Die;
    procedure ClearVisible;
  public
    TeleportedTimes: Integer;
    PlayerMovementMap, EnemyMovementMap: TMovementMap;
    EnemyVector: TVector2;
    TreasureVector: TVector2;
    EnemiesCanSeePlayer: Boolean;
    TotalEnemiesCanSeePlayer: Integer;

    TilesDiscovered: QWord; // Overflow here is possible
    AlertPoint: TCoordinateVector;
    AlertTimeout: Integer;
    SafetyTimeout: Integer;
    IsAlive: Boolean;
    TreasureInventory: array [0..5] of Integer;
    TreasureFound: Integer;
    X, Y: TCoordinate;
    OnStateChanged: TNotifyEvent;
    IsHighScore: Boolean;
    Tile: Integer;
    function Score: Integer;
    procedure Alert(const Timeout: Integer);
    function CanSeeNow(const AX, AY: TCoordinate): Boolean;
    procedure LookAround;
    function TryMove(const DX, DY: Shortint): Boolean;
    function FollowMovementMap: Boolean;
    procedure TryTeleport(const Debug: Boolean);
    function Context: TContext;
    function HasTeleports: Integer;
    procedure Save(const Element: TDOMElement);
    class function Load(const Element: TDOMElement): TPlayer;
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  Player: TPlayer;

implementation
uses
  Generics.Collections,
  CastleLog, CastleConfig, CastleXmlUtils,
  CastleApplicationProperties,
  GameMap, GameAchievements, GameMapChunk, GameMapObject, GameSave,
  GameSounds, GameVibrate;

procedure TPlayer.Save(const Element: TDOMElement);
var
  ThisElement: TDOMElement;
begin
  ThisElement := Element.CreateChild('player');
  ThisElement.AttributeSet('TeleportedTimes', TeleportedTimes);
  ThisElement.AttributeSet('EnemyVector', EnemyVector);
  ThisElement.AttributeSet('TreasureVector', TreasureVector);
  ThisElement.AttributeSet('EnemiesCanSeePlayer', EnemiesCanSeePlayer);
  ThisElement.AttributeSet('TotalEnemiesCanSeePlayer', TotalEnemiesCanSeePlayer);
  ThisElement.AttributeSet('TilesDiscovered', TilesDiscovered);

  ThisElement.AttributeSet('AlertTimeout', AlertTimeout);
  ThisElement.AttributeSet('SafetyTimeout', SafetyTimeout);
  ThisElement.AttributeSet('IsAlive', IsAlive);
  ThisElement.AttributeSet('Tile', Tile);

  ThisElement.AttributeSet('Inventory0', TreasureInventory[0]);
  ThisElement.AttributeSet('Inventory1', TreasureInventory[1]);
  ThisElement.AttributeSet('Inventory2', TreasureInventory[2]);
  ThisElement.AttributeSet('Inventory3', TreasureInventory[3]);
  ThisElement.AttributeSet('Inventory4', TreasureInventory[4]);
  ThisElement.AttributeSet('Inventory5', TreasureInventory[5]);

  ThisElement.AttributeSet('TreasureFound', TreasureFound);
  ThisElement.AttributeSet('X', X);
  ThisElement.AttributeSet('Y', Y);
  ThisElement.AttributeSet('IsHighScore', IsHighScore);

  ThisElement.AttributeSet('AlertX', AlertPoint.X);
  ThisElement.AttributeSet('AlertY', AlertPoint.Y);
end;

class function TPlayer.Load(const Element: TDOMElement): TPlayer;
begin
  Result := TPlayer.Create;

  Result.TeleportedTimes := Element.AttributeInteger('TeleportedTimes');
  Result.EnemyVector := Element.AttributeVector2('EnemyVector');
  Result.TreasureVector := Element.AttributeVector2('TreasureVector');
  Result.EnemiesCanSeePlayer := Element.AttributeBoolean('EnemiesCanSeePlayer');
  Result.TotalEnemiesCanSeePlayer := Element.AttributeInteger('TotalEnemiesCanSeePlayer');
  Result.TilesDiscovered := Element.AttributeQWord('TilesDiscovered');

  Result.AlertTimeout := Element.AttributeInteger('AlertTimeout');
  Result.SafetyTimeout := Element.AttributeInteger('SafetyTimeout');
  Result.IsAlive := Element.AttributeBoolean('IsAlive');
  Result.Tile := Element.AttributeInteger('Tile');
  UserConfig.SetValue('last_player_tile', Result.Tile); // just to make sure Create didn't mess it up

  Result.TreasureInventory[0] := Element.AttributeInteger('Inventory0');
  Result.TreasureInventory[1] := Element.AttributeInteger('Inventory1');
  Result.TreasureInventory[2] := Element.AttributeInteger('Inventory2');
  Result.TreasureInventory[3] := Element.AttributeInteger('Inventory3');
  Result.TreasureInventory[4] := Element.AttributeInteger('Inventory4');
  Result.TreasureInventory[5] := Element.AttributeInteger('Inventory5');

  Result.TreasureFound := Element.AttributeInteger('TreasureFound');
  Result.X := Element.AttributeInt64('X');
  Result.Y := Element.AttributeInt64('Y');
  Result.IsHighScore := Element.AttributeBoolean('IsHighScore');

  Result.AlertPoint.X := Element.AttributeInt64('AlertX');
  Result.AlertPoint.Y := Element.AttributeInt64('AlertY');
  if Result.AlertTimeout > 0 then
    Result.EnemyMovementMap.BuildMovementMap(Result.AlertPoint, Result.AlertTimeout * 2, true, CoordinateVector(0, 0), false);
end;

constructor TPlayer.Create;
var
  I: Integer;
begin
  //inherited --- Parent is not virtual;
  EnemyMovementMap := TMovementMap.Create;
  EnemyMovementMap.Size := 32;
  PlayerMovementMap := TMovementMap.Create;
  PlayerMovementMap.Size := 512;
  TreasureFound := 0;
  TilesDiscovered := 0;
  TeleportedTimes := 0;
  FCachedHighScore := UserConfig.GetValue('high_score', 0);
  IsAlive := true;
  repeat
    Tile := Xorshift.Random(6);
  until Tile <> UserConfig.GetValue('last_player_tile', 0);
  UserConfig.SetValue('last_player_tile', Tile);
  IsHighScore := false;
  AlertTimeout := -1;
  SafetyTimeout := EnemySurpriseSafetyDuration;
  for I := 0 to 5 do
    TreasureInventory[I] := 0;
end;

destructor TPlayer.Destroy;
begin
  PlayerMovementMap.Free;
  EnemyMovementMap.Free;
  inherited;
end;

function TPlayer.Score: Integer;
begin
  Result := TilesDiscovered + TreasureFound * 1000;
end;

procedure TPlayer.Die;
var
  PlayerDeaths: Integer;
//  I: Integer;
begin
  DeleteSaveGame;
  IsAlive := false;
  PlayerDeaths := UserConfig.GetInteger('player_deaths', 0);
  Inc(PlayerDeaths);
  UserConfig.SetValue('player_deaths', PlayerDeaths);
  IsHighScore := Score > UserConfig.GetValue('high_score', 0);
  if IsHighScore then
  begin
    UserConfig.SetValue('high_score', Score);
    FCachedHighScore := Score;
  end;
  UserConfig.Save;

  if PlayerDeaths = 1 then
    SetAchievement('die_1');
  if PlayerDeaths = 12 then
    SetAchievement('die_12');
  if PlayerDeaths = 32 then
    SetAchievement('die_32');
  if PlayerDeaths = 64 then
    SetAchievement('die_64');

  if HasTeleports > 0 then
    SetAchievement('die_teleport');
  if HasTeleports >= 3 then
    SetAchievement('die_teleport_3');

  if Score <= 6000 then
    SetAchievement('rebel');

  ClearVisible;
  WriteLnLog('Tiles discovered = ' + IntToStr(TilesDiscovered));
  WriteLnLog('Treasures found = ' + IntToStr(TreasureFound));
  //for I := 0 to 15 do
  //  WriteLnLog('Biome ' + IntToStr(I) + ' treasures = ' + IntToStr(DungeonMap.TreasureStatistics[I]) + ' / enemies = ' + IntToStr(DungeonMap.EnemiesStatistics[I]));
  WriteLnLog('Score = ' + IntToStr(Score));
  OnStateChanged(Self);

  Vibrate(500);
end;

function TPlayer.Context: TContext;
begin
  Result := TContext.Create;
  if ApplicationProperties.TouchDevice then
    Result.ALLOW.Add('TOUCH');
  if AlertTimeout > 0 then
    Result.ALLOW.Add('DANGER');
  if HasTeleports > 0 then
    Result.ALLOW.Add('TELEPORT');
  if EnemiesCanSeePlayer then
    Result.ALLOW.Add('MONSTER');
  if TotalEnemiesCanSeePlayer >= 3 then
    Result.ALLOW.Add('MANYMONSTERS');
  if not IsAlive then
    Result.DEMAND.Add('DEAD');
  if Score < 10000 then
    Result.ALLOW.Add('START');
  if Score < 7000 then
    Result.ALLOW.Add('BEGIN');
  if Score > 25000 then
    Result.ALLOW.Add('ADVANCED');
  if (UserConfig.GetValue('high_score', 0) > 0) and (Score > UserConfig.GetValue('high_score', 0)) then
    Result.ALLOW.Add('HIGHSCORE');
  if FCachedHighScore < 15000 then
    Result.ALLOW.Add('NEWBIE');
  if FCachedHighScore > 40000 then
    Result.ALLOW.Add('EXPERT');
  case DungeonMap.GetBiome(X, Y) of
    BiomeForest: Result.ALLOW.Add('FOREST');
    BiomeSwamp: Result.ALLOW.Add('SWAMP');
    BiomeCrimson: Result.ALLOW.Add('CRIMSON');
    BiomeSand: Result.ALLOW.Add('SAND');
    BiomeBlue: Result.ALLOW.Add('BLUE');
    BiomeRock: Result.ALLOW.Add('ROCK');
    BiomeCastle: Result.ALLOW.Add('CASTLE');
    BiomeCatacombs: Result.ALLOW.Add('CATACOMBS');
    BiomeMetal: Result.ALLOW.Add('METAL');
    BiomeBlack: Result.ALLOW.Add('BLACK');
    BiomeVault: Result.ALLOW.Add('VAULT');
    BiomePrison: Result.ALLOW.Add('PRISON');
    BiomeMossy: Result.ALLOW.Add('MOSS');
    BiomeCrystal: Result.ALLOW.Add('STRUCTURE');
    BiomeMarble: Result.ALLOW.Add('STRUCTURE');
    BiomeLab: Result.ALLOW.Add('STRUCTURE');
    else
      raise Exception.Create('Unexpected player biome: ' + DungeonMap.GetBiome(X, Y).ToString);
  end;
end;

function TPlayer.CanSeeNow(const AX, AY: TCoordinate): Boolean;
begin
  if (AX - X >= -VisibilityRange) and (AX - X <= VisibilityRange) and
     (AY - Y >= -VisibilityRange) and (AY - Y <= VisibilityRange) then
    Result := Visibility[AX - X, AY - Y]
  else
    Result := false;
end;

procedure TPlayer.ClearVisible;
var
  IX, IY: Integer;
begin
  for IX := -VisibilityRange to VisibilityRange do
    for IY := -VisibilityRange to VisibilityRange do
      Visibility[IX, IY] := false;
end;

procedure TPlayer.Alert(const Timeout: Integer);
begin
  if (AlertTimeout <= 0) and (Timeout > 0) then
    OnStateChanged(Self);
  AlertTimeout := Timeout;
  AlertPoint.X := X;
  AlertPoint.Y := Y;
  EnemyMovementMap.BuildMovementMap(AlertPoint, Timeout * 2, true, CoordinateVector(0, 0), false);
end;

procedure TPlayer.LookAround;

  function Sign(const A: Integer): Integer; inline;
  begin
    if A > 0 then
      Result := 1
    else
    if A < 0 then
      Result := -1
    else
      Result := 0;
  end;

var
  DX, DY, IX, IY: Integer;
  Slope: Single;
begin
  ClearVisible;

  for IX := -VisibilityRange to VisibilityRange do
    for IY := -VisibilityRange to VisibilityRange do
      if Sqr(IX) + Sqr(IY) <= Sqr(VisibilityRange) then
        if (IX = 0) and (IY = 0) then
        begin
          if not DungeonMap.GetVisible(X, Y) then
            Inc(TilesDiscovered);
          DungeonMap.SetVisible(X, Y);
          Visibility[0, 0] := true;
        end else
        if Abs(IX) > Abs(IY) then
        begin
          DX := 0;
          DY := 0;
          Slope := Single(IY) / Single(IX);
          repeat
            DX += Sign(IX);
            if not DungeonMap.GetVisible(X + DX, Y + DY) then
              Inc(TilesDiscovered);
            DungeonMap.SetVisible(X + DX, Y + DY);
            Visibility[DX, DY] := true;
            if not DungeonMap.CanPass(X + DX, Y + DY) then
              break;
            if Trunc(DX * Slope) <> Trunc((DX + Sign(IX)) * Slope) then
            begin
              DY += Sign(IY);
              if not DungeonMap.GetVisible(X + DX, Y + DY) then
                Inc(TilesDiscovered);
              DungeonMap.SetVisible(X + DX, Y + DY);
              Visibility[DX, DY] := true;
              if not DungeonMap.CanPass(X + DX, Y + DY) then
                break;
            end;
          until (DX = IX) and (DY = IY);
        end else
        begin
          DX := 0;
          DY := 0;
          Slope := Single(IX) / Single(IY);
          repeat
            DY += Sign(IY);
            if not DungeonMap.GetVisible(X + DX, Y + DY) then
              Inc(TilesDiscovered);
            DungeonMap.SetVisible(X + DX, Y + DY);
            Visibility[DX, DY] := true;
            if not DungeonMap.CanPass(X + DX, Y + DY) then
              break;
            if Trunc(DY * Slope) <> Trunc((DY + Sign(IY)) * Slope) then
            begin
              DX += Sign(IX);
              if not DungeonMap.GetVisible(X + DX, Y + DY) then
                Inc(TilesDiscovered);
              DungeonMap.SetVisible(X + DX, Y + DY);
              Visibility[DX, DY] := true;
              if not DungeonMap.CanPass(X + DX, Y + DY) then
                break;
            end;
          until (DX = IX) and (DY = IY);
        end;
  DungeonMap.GenerateChunksAroundPlayer(false, 1);
end;

function TPlayer.TryMove(const DX, DY: Shortint): Boolean;

  procedure CheckTreasure;
  var
    Treasure: TMapObject;
    CX, CY: Integer;
    V: TVector2;
    L: Single;
    OldTeleports: Integer;
  begin
    TreasureVector := Vector2(0, 0);
    for CX := -1 to 1 do
      for CY := -1 to 1 do
        for Treasure in DungeonMap.GetChunkAroundPlayer(CX, CY).Treasure do
        begin
          if (Treasure.X = X) and (Treasure.Y = Y) then
          begin
            OldTeleports := HasTeleports;
            TreasureInventory[Treasure.Tile] += 1;
            if HasTeleports > OldTeleports then
              Sound('teleport_available')
            else
              Sound('pick_up_treasure');
            Inc(TreasureFound);
            DungeonMap.GetChunkAroundPlayer(CX, CY).Treasure.Remove(Treasure);
            Alert(TreasureAlertDuration);
          end else
          begin
            V := Vector2(Treasure.X - X, Treasure.Y - Y);
            L := V.LengthSqr;
            if (L <= Sqr(ChunkSize)) then
              TreasureVector += V / Sqr(L); // By the condition above we guarantee that LengthSqr is >= 1
          end;
        end;
  end;

  procedure CheckEnemies;
  var
    Enemy: TEnemy;
    EnemyCanSeeNowFixed: Boolean;
    CX, CY: Integer;
    DPX, DPY: TCoordinate;
    V: TVector2;
    L: Single;
    AiMovementDirection: TMovementDirection;

    function Sign(const A: TCoordinate): Shortint;
    begin
      if A > 0 then
        Exit(1)
      else
      if A < 0 then
        Exit(-1)
      else
        Exit(0);
    end;

  begin
    for CX := -1 to 1 do
      for CY := -1 to 1 do
        for Enemy in DungeonMap.GetChunkAroundPlayer(CX, CY).Enemies do
          Enemy.ReadyToMove := true;
    TotalEnemiesCanSeePlayer := 0;
    EnemyVector := Vector2(0, 0);
    for CX := -1 to 1 do
      for CY := -1 to 1 do
        for Enemy in DungeonMap.GetChunkAroundPlayer(CX, CY).Enemies do
          if Enemy.ReadyToMove then // to avoid enemy move twice when switching chunk
          begin
            V := Vector2(Enemy.X - X, Enemy.Y - Y);
            L := V.LengthSqr;
            if (L <= Sqr(ChunkSize)) then
              EnemyVector += V / Sqr(L);

            EnemyCanSeeNowFixed := CanSeeNow(Enemy.X, Enemy.Y);
            if (CanSeeNow(Enemy.X, Enemy.Y) or (AlertTimeout > 0)) then
            begin
              if CanSeeNow(Enemy.X, Enemy.Y) then
              begin
                Alert(EnemyAlertDuration);
                EnemiesCanSeePlayer := true;
              end;
              if SafetyTimeout <= 0 then
              begin
                AiMovementDirection := EnemyMovementMap.GetMovementMapSafe(Enemy.X, Enemy.Y);
                if AiMovementDirection > mdUndefined then
                begin
                  //fancy pathfinding algorithm
                  case AiMovementDirection of
                    mdUp: Enemy.Move(0, 1);
                    mdDown: Enemy.Move(0, -1);
                    mdLeft: Enemy.Move(-1, 0);
                    mdRight: Enemy.Move(1, 0);
                  end;
                end else
                begin
                  //simple dumb algorithm - move one step in direction of the player
                  DPX := AlertPoint.X - Enemy.X;
                  DPY := AlertPoint.Y - Enemy.Y;
                  if (Abs(DPX) > Abs(DPY)) or ((Abs(DPX) = Abs(DPY)) and Xorshift.RandomBoolean) then
                  begin
                    if not Enemy.Move(Sign(DPX), 0) then
                      Enemy.Move(0, Sign(DPY));
                  end else
                  begin
                    if not Enemy.Move(0, Sign(DPY)) then
                      Enemy.Move(Sign(DPX), 0);
                  end;
                end;
                if CanSeeNow(Enemy.X, Enemy.Y) then
                begin
                  Alert(EnemyAlertDuration);
                  EnemiesCanSeePlayer := true;
                end;
              end;
              if (Enemy.X = X) and (Enemy.Y = Y) then
              begin
                Die;
                Exit;
              end;
            end else
            begin
              // drunken walker
              DPX := Xorshift.RandomSign;
              if Xorshift.RandomBoolean then
                Enemy.Move(DPX, 0)
              else
                Enemy.Move(0, DPX);
              if CanSeeNow(Enemy.X, Enemy.Y) then
              begin
                Alert(EnemyAlertDuration);
                EnemiesCanSeePlayer := true;
              end;
            end;
            EnemyCanSeeNowFixed := EnemyCanSeeNowFixed or CanSeeNow(Enemy.X, Enemy.Y);
            if EnemyCanSeeNowFixed then
              Inc(TotalEnemiesCanSeePlayer);
          end;
    //WriteLnLog(TotalEnemiesCanSeePlayer.ToString);
    if TotalEnemiesCanSeePlayer >= 3 then
      SetAchievement('monsters_3');
    if TotalEnemiesCanSeePlayer >= 5 then
      SetAchievement('monsters_5');
    if TotalEnemiesCanSeePlayer >= 7 then
      SetAchievement('monsters_7');
    // Maybe the player is surrounded? -- bad temporary solution for end game // still the player can be blocked in 2x2 room "dancing" with the monster without being able to die or flee
    {if EnemiesCanSeePlayer then
      if (not DungeonMap.CanPassEnemy(X + 1, Y)) and (not DungeonMap.CanPassEnemy(X, Y + 1)) and
         (not DungeonMap.CanPassEnemy(X - 1, Y)) and (not DungeonMap.CanPassEnemy(X, Y - 1)) then
           Die; }

    if AlertTimeout > 0 then
      Dec(SafetyTimeout)
    else
    begin
      if SafetyTimeout <= EnemySurpriseSafetyDuration then
        SafetyTimeout := EnemySurpriseSafetyDuration
      else
        if Xorshift.Random < 0.05 then
          Dec(SafetyTimeout);
    end;
  end;

var
  OldBiome: Byte;
  EnemiesAround: Boolean;
begin
  if IsAlive and DungeonMap.CanPass(Player.X + DX, Player.Y + DY) then
  begin
    Result := true;
    OldBiome := DungeonMap.GetBiome(X, Y);
    Player.X += DX;
    Player.Y += DY;
    LookAround;
    CheckTreasure;
    EnemiesAround := EnemiesCanSeePlayer;
    EnemiesCanSeePlayer := false;
    if (AlertTimeout > 0) then
    begin
      Dec(AlertTimeout);
      if AlertTimeout = 0 then
        OnStateChanged(Self);
    end;
    CheckEnemies;
    if EnemiesCanSeePlayer and not EnemiesAround then
    begin
      Sound('danger');
      Vibrate(100);
    end;
    if (EnemiesAround <> EnemiesCanSeePlayer) or (OldBiome <> DungeonMap.GetBiome(X, Y)) then
      OnStateChanged(Self);

    if Score >= 6000 then
      SetAchievement('score_6k');
    if Score >= 12000 then
      SetAchievement('score_12k');
    if Score >= 20000 then
      SetAchievement('score_20k');
    if Score >= 35000 then
      SetAchievement('score_35k');
    if Score >= 50000 then
      SetAchievement('score_50k');
    if TilesDiscovered >= 2000 then
      SetAchievement('travel_2k');
    if TilesDiscovered >= 7000 then
      SetAchievement('travel_7k');
    if TilesDiscovered >= 15000 then
      SetAchievement('travel_15k');
  end else
    Result :=false;
end;

function TPlayer.FollowMovementMap: Boolean;
begin
  case PlayerMovementMap.GetMovementMapSafe(X, Y) of
    mdUp: Result := TryMove(0, 1);
    mdDown: Result := TryMove(0, -1);
    mdLeft: Result := TryMove(-1, 0);
    mdRight: Result := TryMove(1, 0);
    else
      Result := false;
  end;
end;

type
  TTeleportSpot = record
    X, Y: TCoordinate;
    SqrNearestEnemyDistance: Single;
  end;

function TPlayer.HasTeleports: Integer;
var
  I: Integer;
begin
  Result := MaxInt;
  for I := 0 to 5 do
    if TreasureInventory[I] < Result then
      Result := TreasureInventory[I];
end;

procedure TPlayer.TryTeleport(const Debug: Boolean);
var
  IX, IY, JX, JY: Integer;
  Enemy: TEnemy;
  TeleportRange: Integer;
  D: Single;
  SqrDistanceToNearestEnemy, BestSqrDistanceToNearestEnemy: Single;
  I: Integer;
  PossibleTeleportSpots: specialize TList<TTeleportSpot>;
  TeleportSpot: TTeleportSpot;
begin
  if (IsAlive and EnemiesCanSeePlayer and (HasTeleports > 0)) or Debug then
  begin
    if not Debug then
    begin
      TeleportRange := MaxTeleportRange;
      PossibleTeleportSpots := specialize TList<TTeleportSpot>.Create;
      repeat
        BestSqrDistanceToNearestEnemy := 0;
        //PossibleTeleportSpots.Clear;
        for IX := -TeleportRange to TeleportRange do
          for IY := -TeleportRange to TeleportRange do
          begin
            D := Sqr(IX) + Sqr(IY);
            if (D > Sqr(MinTeleportRange)) and (D < Sqr(MaxTeleportRange)) and
              DungeonMap.CanTeleportHere(Player.X + IX, Player.Y + IY) and
              (not DungeonMap.GetVisible(Player.X + IX, Player.Y + IY)) then
            begin
              SqrDistanceToNearestEnemy := Sqr(TeleportRange);
              for JX := -TeleportRange div ChunkSize - 1 to TeleportRange div ChunkSize + 1 do
                if SqrDistanceToNearestEnemy > Sqr(SafeTeleportArea) then
                  for JY := -TeleportRange div ChunkSize - 1 to TeleportRange div ChunkSize + 1 do
                    if (SqrDistanceToNearestEnemy > Sqr(SafeTeleportArea)) and
                      (DungeonMap.GetChunkAroundPlayer(JX, JY) <> nil) then
                        for Enemy in DungeonMap.GetChunkAroundPlayer(JX, JY).Enemies do //inefficient but ok
                        begin
                          D := Sqr(Enemy.X - (Player.X + IX)) + Sqr(Enemy.Y - (Player.Y + IY));
                          if (D < SqrDistanceToNearestEnemy) then
                            SqrDistanceToNearestEnemy := D;
                          if SqrDistanceToNearestEnemy < Sqr(SafeTeleportArea) then
                            break;
                        end;
              if SqrDistanceToNearestEnemy > Sqr(SafeTeleportArea) then
              begin
                TeleportSpot.X := Player.X + IX;
                TeleportSpot.Y := Player.Y + IY;
                TeleportSpot.SqrNearestEnemyDistance := SqrDistanceToNearestEnemy;
                PossibleTeleportSpots.Add(TeleportSpot);
                if BestSqrDistanceToNearestEnemy < SqrDistanceToNearestEnemy then
                  BestSqrDistanceToNearestEnemy := SqrDistanceToNearestEnemy;
              end;
            end;
          end;
        if PossibleTeleportSpots.Count = 0 then
          TeleportRange *= 2;
        WriteLnLog('N teleport spots = ' + PossibleTeleportSpots.Count.ToString + ' / Best Distance to Enemy = ' + Sqrt(BestSqrDistanceToNearestEnemy).ToString);
      until PossibleTeleportSpots.Count > 0;
      //BUG: sometimes map generates impassable chunks
      for I := 0 to 5 do
        Dec(TreasureInventory[I]);
      I := 0;
      repeat
        if PossibleTeleportSpots[I].SqrNearestEnemyDistance < BestSqrDistanceToNearestEnemy / 2 then
          PossibleTeleportSpots.Delete(I)
        else
          Inc(I);
      until I = PossibleTeleportSpots.Count;
      WriteLnLog('N good teleport spots = ' + PossibleTeleportSpots.Count.ToString);
      I := Xorshift.Random(PossibleTeleportSpots.Count);
      Player.X := PossibleTeleportSpots[I].X;
      Player.Y := PossibleTeleportSpots[I].Y;
      PossibleTeleportSpots.Free;
    end else
    begin
      repeat
        TeleportSpot.X := (High(TCoordinate) div 2 div ChunkSize) * ChunkSize + ChunkSize div 2 + Xorshift.Random(4096) - 4096 div 2;
        TeleportSpot.Y := (High(TCoordinate) div 2 div ChunkSize) * ChunkSize + ChunkSize div 2 + Xorshift.Random(4096) - 4096 div 2;
      until DungeonMap.CanTeleportHere(TeleportSpot.X, TeleportSpot.Y);
      Player.X := TeleportSpot.X;
      Player.Y := TeleportSpot.Y;
    end;

    SafetyTimeout := PostTeleoprtSafetyDuration;
    AlertTimeout := -1;
    TryMove(0, 0);

    Inc(TeleportedTimes);
    if TeleportedTimes = 1 then
      SetAchievement('teleport_1');
    if TeleportedTimes = 3 then
      SetAchievement('teleport_3');
    if TeleportedTimes = 5 then
      SetAchievement('teleport_5');

    OnStateChanged(Self);
    if not Debug then
      Sound('teleport');
    SaveGame;
  end;
end;

end.

