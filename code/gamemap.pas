{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Map generator and manager
  the main idea of the semi-infinite map generation is to split the map
  into chunks and generate new chunks (as a continuation of already generated parts)
  only when they are requested.
  There is no memory manager and capability to load/unload chunks on the go,
  therefore this map is significantly limited by available RAM. }
unit GameMap;

{$mode objfpc}{$H+}

interface
uses
  Generics.Collections, DOM,
  Global, GameMapObject, GameMapChunk;

const
  MaxTiles = 30;
  MaxEnemies = 16;
  MaxSigils = 3;
  MaxMaxRoomSize = 7 + 2;
  GlobalSigilChance = 0.03;

type
  TBiomeAlgorithm = (baPass, baStrictPass, baBox, baHedgehog);

type
  TBiome = record
    RangeMultiplier: Single; //the larger - the smaller biome
    WallChance: Single;
    MinSize, MaxSize: Integer;
    CanPass: Boolean;
    Algorithm: TBiomeAlgorithm;
    RoomChance: Single;
    StructureChance: Single;
    MinRoomSize, MaxRoomSize: Integer;
    EnemyChance: Single;
    SigilChance: Single;
  end;

const
  BiomeForest = 0;
  BiomeSwamp = 2; // wroms
  BiomeCrimson = 4;
  BiomeSand = 6;
  BiomeBlue = 8;
  BiomeRock = 10;
  BiomeCastle = 12;
  BiomeCatacombs = 14;
  BiomeMetal = 16;
  BiomeBlack = 18;
  BiomeVault = 20; // todo
  BiomePrison = 22; // todo
  BiomeMossy = 24; // todo
  BiomeCrystal = 26; // structures
  BiomeMarble = 28;
  BiomeLab = 30;
  MaxRandomBiome = BiomeMossy div 2 + 1; // TODO: BiomeMossy
  MaxBiomeCount = BiomeLab + 1;

type
  TMap = class(TObject)
  strict private
    TotalAnchorPoints: QWord;
    Biomes: array [0..MaxBiomeCount] of TBiome;
    function GetChunkBiome(const X, Y: TCoordinate): Byte;
    procedure GenerateChunk(AChunk: TChunk);
    procedure WriteMap(const X, Y: TCoordinate; const ABiome: Byte);
    procedure AddAnchorPoint(const X, Y: TCoordinate);
    function GetBiomeSafe(const X, Y: TCoordinate): Byte;
    procedure EmergencyDungeonRecovery;
  public
    Chunks: TChunksDictionary;
    EnemiesStatistics, TreasureStatistics: array [0..MaxBiomeCount] of Integer;
    procedure GeneratePlayer;
    procedure GenerateChunksAroundPlayer(const GameStart: Boolean; const MaxDiff: Integer);
    function GetBiome(const X, Y: TCoordinate): Byte;
    function GetTile(const X, Y: TCoordinate): Byte;
    function GetVisible(const X, Y: TCoordinate): Boolean;
    function FindNearestGoToDestination(const AVector: TCoordinateVector): TCoordinateVector;
    function CanPassEnemy(const X, Y: TCoordinate): Boolean;
    { same as CanPassEnemy but also no treasure allowed }
    function CanTeleportHere(const X, Y: TCoordinate): Boolean;
    function CanPass(const X, Y: TCoordinate): Boolean;
    function CanPassPlayer(const X, Y: TCoordinate): Boolean;
    function GetChunkAroundPlayer(const DX, DY: Integer): TChunk;
    procedure SetVisible(const X, Y: TCoordinate);
    procedure DebugSetAllVisible;
    procedure Save(const Element: TDOMElement);
    class function Load(const Element: TDOMElement): TMap;
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  DungeonMap: TMap;

implementation
uses
  SysUtils,
  CastleLog, CastleXmlUtils,
  GamePlayer, GameMapStructures;

procedure TMap.Save(const Element: TDOMElement);
var
  ThisElement: TDOMElement;
  V: TCoordinateVector;
begin
  ThisElement := Element.CreateChild('map');
  for V in Chunks.Keys do
    Chunks[V].Save(ThisElement, V);
  ThisElement.AttributeSet('TotalAnchorPoints', TotalAnchorPoints);
end;

class function TMap.Load(const Element: TDOMElement): TMap;
var
  I: TXMLElementIterator;
  V: TCoordinateVector;
begin
  Result := TMap.Create;
  I := Element.ChildrenIterator('chunk');
  try
    while I.GetNext do
    begin
      V := CoordinateVector(I.Current.AttributeInt64('X'), I.Current.AttributeInt64('Y'));
      Result.Chunks.Add(V, TChunk.Load(I.Current));
    end;
  finally
    FreeAndNil(I)
  end;
  Result.TotalAnchorPoints := Element.AttributeQWord('TotalAnchorPoints');
end;

constructor TMap.Create;
var
  I: Integer;
begin
  //inherited - Parent isn't virtual;

  for I := 0 to 15 do
  begin
    EnemiesStatistics[I] := 0;
    TreasureStatistics[I] := 0;
  end;

  Chunks := TChunksDictionary.Create([doOwnsValues]);

  // biome 14 - Middle-sized chaotic corridors
  Biomes[BiomeCatacombs + 1].CanPass := false;
  Biomes[BiomeCatacombs].CanPass := true;
  Biomes[BiomeCatacombs].RangeMultiplier := 0.8;
  Biomes[BiomeCatacombs].MinSize := 0;
  Biomes[BiomeCatacombs].MaxSize := 4;
  Biomes[BiomeCatacombs].Algorithm := baPass;
  Biomes[BiomeCatacombs].WallChance := 0.40;
  Biomes[BiomeCatacombs].StructureChance := 0.01;
  Biomes[BiomeCatacombs].RoomChance := 0.06;
  Biomes[BiomeCatacombs].MinRoomSize := 2;
  Biomes[BiomeCatacombs].MaxRoomSize := 3;
  Biomes[BiomeCatacombs].EnemyChance := 0.003;
  Biomes[BiomeCatacombs].SigilChance := GlobalSigilChance;

  // biome 12 - Long organized corridors
  Biomes[BiomeCastle + 1].CanPass := false;
  Biomes[BiomeCastle].CanPass := true;
  Biomes[BiomeCastle].RangeMultiplier := 0.9;
  Biomes[BiomeCastle].MinSize := 2;
  Biomes[BiomeCastle].MaxSize := 8;
  Biomes[BiomeCastle].Algorithm := baStrictPass;
  Biomes[BiomeCastle].WallChance := 0.39;
  Biomes[BiomeCastle].StructureChance := 0.02;
  Biomes[BiomeCastle].RoomChance := 0.15;
  Biomes[BiomeCastle].MinRoomSize := 2;
  Biomes[BiomeCastle].MaxRoomSize := 4;
  Biomes[BiomeCastle].EnemyChance := 0.010;
  Biomes[BiomeCastle].SigilChance := GlobalSigilChance;

  // biome 10 - Random passages with occasional empty areas
  Biomes[BiomeRock + 1].CanPass := false;
  Biomes[BiomeRock].CanPass := true;
  Biomes[BiomeRock].RangeMultiplier := 1;
  Biomes[BiomeRock].MinSize := 0;
  Biomes[BiomeRock].MaxSize := 2;
  Biomes[BiomeRock].Algorithm := baBox;
  Biomes[BiomeRock].WallChance := 0.55;
  Biomes[BiomeRock].StructureChance := 0.005;
  Biomes[BiomeRock].RoomChance := 0.02;
  Biomes[BiomeRock].MinRoomSize := 2;
  Biomes[BiomeRock].MaxRoomSize := 2;
  Biomes[BiomeRock].EnemyChance := 0.004;
  Biomes[BiomeRock].SigilChance := 0;

  // biome 8 - Big rooms
  Biomes[BiomeBlue + 1].CanPass := false;
  Biomes[BiomeBlue].CanPass := true;
  Biomes[BiomeBlue].RangeMultiplier := 3;
  Biomes[BiomeBlue].MinSize := 2;
  Biomes[BiomeBlue].MaxSize := 3;
  Biomes[BiomeBlue].Algorithm := baBox;
  Biomes[BiomeBlue].WallChance := 0.84;
  Biomes[BiomeBlue].StructureChance := 0;
  Biomes[BiomeBlue].RoomChance := 0.4;
  Biomes[BiomeBlue].MinRoomSize := 2;
  Biomes[BiomeBlue].MaxRoomSize := 4;
  Biomes[BiomeBlue].EnemyChance := 0.015;
  Biomes[BiomeRock].SigilChance := 0;

  // biome 6 - Short chaotic passages
  Biomes[BiomeSand + 1].CanPass := false;
  Biomes[BiomeSand].CanPass := true;
  Biomes[BiomeSand].RangeMultiplier := 1;
  Biomes[BiomeSand].MinSize := 0;
  Biomes[BiomeSand].MaxSize := 1;
  Biomes[BiomeSand].Algorithm := baPass;
  Biomes[BiomeSand].WallChance := 0.34;
  Biomes[BiomeSand].StructureChance := 0.01;
  Biomes[BiomeSand].RoomChance := 0.02;
  Biomes[BiomeSand].MinRoomSize := 2;
  Biomes[BiomeSand].MaxRoomSize := 2;
  Biomes[BiomeSand].EnemyChance := 0.003;
  Biomes[BiomeSand].SigilChance := 0;

  // biome 4 - Short organized corridors
  Biomes[BiomeCrimson + 1].CanPass := false;
  Biomes[BiomeCrimson].CanPass := true;
  Biomes[BiomeCrimson].RangeMultiplier := 4;
  Biomes[BiomeCrimson].MinSize := 1;
  Biomes[BiomeCrimson].MaxSize := 1;
  Biomes[BiomeCrimson].Algorithm := baStrictPass;
  Biomes[BiomeCrimson].WallChance := 0.35;
  Biomes[BiomeCrimson].StructureChance := 0;
  Biomes[BiomeCrimson].RoomChance := 0.5;
  Biomes[BiomeCrimson].MinRoomSize := 2;
  Biomes[BiomeCrimson].MaxRoomSize := 2;
  Biomes[BiomeCrimson].EnemyChance := 0.060;
  Biomes[BiomeCrimson].SigilChance := GlobalSigilChance;

  // biome 2 - Long not-organized corridors
  Biomes[BiomeSwamp + 1].CanPass := false;
  Biomes[BiomeSwamp].CanPass := true;
  Biomes[BiomeSwamp].RangeMultiplier := 1;
  Biomes[BiomeSwamp].MinSize := 0;
  Biomes[BiomeSwamp].MaxSize := 5;
  Biomes[BiomeSwamp].Algorithm := baPass;
  Biomes[BiomeSwamp].WallChance := 0.27;
  Biomes[BiomeSwamp].StructureChance := 0.022;
  Biomes[BiomeSwamp].RoomChance := 0.12;
  Biomes[BiomeSwamp].MinRoomSize := 2;
  Biomes[BiomeSwamp].MaxRoomSize := 3;
  Biomes[BiomeSwamp].EnemyChance := 0.005;
  Biomes[BiomeSwamp].SigilChance := 0;

  // biome 0 - Just random generator
  Biomes[BiomeForest + 1].CanPass := false;
  Biomes[BiomeForest].CanPass := true;
  Biomes[BiomeForest].RangeMultiplier := 0.6;
  Biomes[BiomeForest].MinSize := 0;
  Biomes[BiomeForest].MaxSize := 0;
  Biomes[BiomeForest].Algorithm := baBox;
  Biomes[BiomeForest].WallChance := 0.32;
  Biomes[BiomeForest].StructureChance := 0.003;
  Biomes[BiomeForest].RoomChance := 0.005;
  Biomes[BiomeForest].MinRoomSize := 2;
  Biomes[BiomeForest].MaxRoomSize := 5;
  Biomes[BiomeForest].EnemyChance := 0.0003;
  Biomes[BiomeForest].SigilChance := 0;

  // Same as Crimson, but fewer rooms and monsters
  Biomes[BiomeBlack + 1].CanPass := false;
  Biomes[BiomeBlack].CanPass := true;
  Biomes[BiomeBlack].RangeMultiplier := 2.1;
  Biomes[BiomeBlack].MinSize := 1;
  Biomes[BiomeBlack].MaxSize := 1;
  Biomes[BiomeBlack].Algorithm := baStrictPass;
  Biomes[BiomeBlack].WallChance := 0.35;
  Biomes[BiomeBlack].StructureChance := 0.02;
  Biomes[BiomeBlack].RoomChance := 0.2;
  Biomes[BiomeBlack].MinRoomSize := 2;
  Biomes[BiomeBlack].MaxRoomSize := 4;
  Biomes[BiomeBlack].EnemyChance := 0.005;
  Biomes[BiomeBlack].SigilChance := GlobalSigilChance;

  // Long not-organized corridors
  Biomes[BiomeMetal + 1].CanPass := false;
  Biomes[BiomeMetal].CanPass := true;
  Biomes[BiomeMetal].RangeMultiplier := 1;
  Biomes[BiomeMetal].MinSize := 0;
  Biomes[BiomeMetal].MaxSize := 7;
  Biomes[BiomeMetal].Algorithm := baPass;
  Biomes[BiomeMetal].WallChance := 0.27;
  Biomes[BiomeMetal].StructureChance := 0.06;
  Biomes[BiomeMetal].RoomChance := 0.005;
  Biomes[BiomeMetal].MinRoomSize := 2;
  Biomes[BiomeMetal].MaxRoomSize := 3;
  Biomes[BiomeMetal].EnemyChance := 0.005;
  Biomes[BiomeMetal].SigilChance := GlobalSigilChance;

  // Longer corridors and larger rooms than Crimson, slightly less treasure and monsters
  Biomes[BiomeVault + 1].CanPass := false;
  Biomes[BiomeVault].CanPass := true;
  Biomes[BiomeVault].RangeMultiplier := 3;
  Biomes[BiomeVault].MinSize := 2;
  Biomes[BiomeVault].MaxSize := 2;
  Biomes[BiomeVault].Algorithm := baStrictPass;
  Biomes[BiomeVault].WallChance := 0.35;
  Biomes[BiomeVault].StructureChance := 0.03;
  Biomes[BiomeVault].RoomChance := 0.4;
  Biomes[BiomeVault].MinRoomSize := 3;
  Biomes[BiomeVault].MaxRoomSize := 3;
  Biomes[BiomeVault].EnemyChance := 0.030;
  Biomes[BiomeVault].SigilChance := GlobalSigilChance;

  // Hedgehog map
  Biomes[BiomePrison + 1].CanPass := false;
  Biomes[BiomePrison].CanPass := true;
  Biomes[BiomePrison].RangeMultiplier := 0.8;
  Biomes[BiomePrison].MinSize := 4;
  Biomes[BiomePrison].MaxSize := 6;
  Biomes[BiomePrison].Algorithm := baHedgehog;
  Biomes[BiomePrison].WallChance := 0.0; // Hedgehog map usually has very low chance to branch naturally, so we don't want to miss any opportunity to
  Biomes[BiomePrison].StructureChance := 0.01;
  Biomes[BiomePrison].RoomChance := 0.12;
  Biomes[BiomePrison].MinRoomSize := 2;
  Biomes[BiomePrison].MaxRoomSize := 2;
  Biomes[BiomePrison].EnemyChance := 0.005;
  Biomes[BiomePrison].SigilChance := 0;

  // huge open areas
  Biomes[BiomeMossy + 1].CanPass := false;
  Biomes[BiomeMossy].CanPass := true;
  Biomes[BiomeMossy].RangeMultiplier := 2;
  Biomes[BiomeMossy].MinSize := 3;
  Biomes[BiomeMossy].MaxSize := 7;
  Biomes[BiomeMossy].Algorithm := baBox;
  Biomes[BiomeMossy].WallChance := 0.84;
  Biomes[BiomeMossy].StructureChance := 0.1;
  Biomes[BiomeMossy].RoomChance := 0.0;
  Biomes[BiomeMossy].MinRoomSize := 3;
  Biomes[BiomeMossy].MaxRoomSize := 6;
  Biomes[BiomeMossy].EnemyChance := 0.0024;
  Biomes[BiomeMossy].SigilChance := 0;

  // rooms - no generation parameters
  Biomes[BiomeCrystal + 1].CanPass := false;
  Biomes[BiomeCrystal].CanPass := true;
  Biomes[BiomeCrystal].EnemyChance := 0.004;
  Biomes[BiomeMarble + 1].CanPass := false;
  Biomes[BiomeMarble].CanPass := true;
  Biomes[BiomeMarble].EnemyChance := 0.004;
  Biomes[BiomeLab + 1].CanPass := false;
  Biomes[BiomeLab].CanPass := true;
  Biomes[BiomeLab].EnemyChance := 0.004;

  TotalAnchorPoints := 0;
end;

destructor TMap.Destroy;
begin
  Chunks.Free;
  inherited;
end;

function TMap.GetChunkBiome(const X, Y: TCoordinate): Byte;
var
  V: TCoordinateVector;
  IX, IY: Integer;
  R, R1: Integer;
begin
  R := MaxInt;
  for IX := -1 to 1 do
    for IY := -1 to 1 do
    begin
      V := CoordinateVector(X div ChunkSize + IX, Y div ChunkSize + IY);
      if not Chunks.ContainsKey(V) then
        Chunks.Add(V, TChunk.Create);
      if Chunks[V].BiomeCenterSeed = Undefined then
      begin
        Chunks[V].BiomeCenterSeed := Xorshift.Random(MaxRandomBiome) * 2;
        Chunks[V].BiomeCenterX := (X div ChunkSize + IX) * ChunkSize + Xorshift.Random(ChunkSize);
        Chunks[V].BiomeCenterY := (Y div ChunkSize + IY) * ChunkSize + Xorshift.Random(ChunkSize);
        Chunks[V].BiomeRangeMultiplier := Biomes[Chunks[V].BiomeCenterSeed].RangeMultiplier * (1.0 + (Xorshift.Random - 0.5) * 0.3);
      end;
      R1 := Round(Biomes[Chunks[V].BiomeCenterSeed].RangeMultiplier * Sqr(X - Chunks[V].BiomeCenterX) + Sqr(Y - Chunks[V].BiomeCenterY));
      if R1 < R then
      begin
        R := R1;
        Result := Chunks[V].BiomeCenterSeed;
      end;
    end;
end;

procedure TMap.AddAnchorPoint(const X, Y: TCoordinate);
var
  V, VA: TCoordinateVector;
  CX, CY: Integer;
begin
  V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
  if not Chunks.ContainsKey(V) then
    Chunks.Add(V, TChunk.Create);
  CX := X mod ChunkSize;
  CY := Y mod ChunkSize;
  if Chunks[V].Biome[CX, CY] <> AnchorPoint then
  begin
    VA := CoordinateVector(X, Y);
    Chunks[V].Biome[CX, CY] := AnchorPoint;
    Chunks[V].AnchorPoints.Add(VA);
    Inc(TotalAnchorPoints);
  end;
end;

procedure TMap.WriteMap(const X, Y: TCoordinate; const ABiome: Byte);
var
  V: TCoordinateVector;
  CX, CY: Integer;
begin
  V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
  CX := X mod ChunkSize;
  CY := Y mod ChunkSize;
  if not Chunks.ContainsKey(V) then
    Chunks.Add(V, TChunk.Create);
  if Chunks[V].Biome[CX, CY] = AnchorPoint then
  begin
    Chunks[V].AnchorPoints.Remove(CoordinateVector(X, Y));
    Dec(TotalAnchorPoints);
  end;
  Chunks[V].Biome[CX, CY] := ABiome;
  Chunks[V].Tile[CX, CY] := Xorshift.Random(MaxTiles);
end;

procedure TMap.GeneratePlayer;
const
  EntryRoomSize = 5;
var
  V: TCoordinateVector;
  CX, CY: Integer;
  PX, PY: TCoordinate;
  PlayerBiome: Byte;
  ABiome: Byte;

  procedure BonusTreasure(const DX, DY: Integer; const Tile: Byte);
  var
    Treasure: TMapObject;
  begin
    Treasure := TMapObject.Create;
    Treasure.X := PX + DX;
    Treasure.Y := PY + DY;
    Treasure.Tile := Tile;
    Chunks[V].Treasure.Add(Treasure);
    //Inc(TreasureStatistics[PlayerBiome]); - it doesn't make sense
  end;

begin
  PX := (High(TCoordinate) div 2 div ChunkSize) * ChunkSize + ChunkSize div 2;
  PY := (High(TCoordinate) div 2 div ChunkSize) * ChunkSize + ChunkSize div 2; // start in the middle of a chunk
  Player.X := PX;
  Player.Y := PY;
  V := CoordinateVector(PX div ChunkSize, PY div ChunkSize);
  if not Chunks.ContainsKey(V) then
    Chunks.Add(V, TChunk.Create)
  else
    raise Exception.Create('GeneratePlayer expects the map to be clean.');
  PlayerBiome := BiomeCatacombs;
  Chunks[V].BiomeCenterSeed := PlayerBiome;
  Chunks[V].BiomeCenterX := PX;
  Chunks[V].BiomeCenterY := PY;
  Chunks[V].BiomeRangeMultiplier := 1.0;
  //CX := X mod ChunkSize; // = ChunkSize div 2
  //CY := Y mod ChunkSize;
  for CX := -EntryRoomSize to EntryRoomSize do
    for CY := -EntryRoomSize to EntryRoomSize do
    begin
      if (Abs(CX) = EntryRoomSize) or (Abs(CY) = EntryRoomSize) then
      begin
        if (Abs(CX) = EntryRoomSize div 2) or (Abs(CY) = EntryRoomSize div 2) then
        begin
          AddAnchorPoint(PX + CX, PY + CY);
          continue;
        end else
          ABiome := PlayerBiome + 1;
      end else
        ABiome := PlayerBiome;
      WriteMap(PX + CX, PY + CY, ABiome);
    end;
  // Add starting treasure
  BonusTreasure(-3, 0, 0);
  BonusTreasure( 3, 0, 1);
  BonusTreasure(-2, 3, 2);
  BonusTreasure( 2, 3, 3);
  BonusTreasure(-2,-3, 4);
  BonusTreasure( 2,-3, 5);
  // Generate the map
  GenerateChunksAroundPlayer(true, 1);
  // Remove enemies from the first chunk
  Chunks[V].Enemies.Clear;
  Chunks[CoordinateVector(PX div ChunkSize - 1, PY div ChunkSize)].Enemies.Clear;
  Chunks[CoordinateVector(PX div ChunkSize + 1, PY div ChunkSize)].Enemies.Clear;
  Chunks[CoordinateVector(PX div ChunkSize, PY div ChunkSize - 1)].Enemies.Clear;
  Chunks[CoordinateVector(PX div ChunkSize, PY div ChunkSize + 1)].Enemies.Clear;
end;

procedure TMap.GenerateChunksAroundPlayer(const GameStart: Boolean;
  const MaxDiff: Integer);
var
  ChunksToGenerate: array of TCoordinateVector;
  Quantity: Integer;
  DX, DY: Integer;
  PX, PY: TCoordinate;
  Finished: Boolean;
  I: Integer;
begin
  Quantity := Sqr(MaxDiff * 2 + 1);
  PX := Player.X div ChunkSize;
  PY := Player.Y div ChunkSize;

  ChunksToGenerate := nil;
  SetLength(ChunksToGenerate, Quantity);
  I := 0;
  for DX := -MaxDiff to MaxDiff do
    for DY := -MaxDiff to MaxDiff do
    begin
      ChunksToGenerate[I] := CoordinateVector(PX + DX, PY + DY);
      Inc(I);
    end;

  for I := 0 to Pred(Quantity) do
    if not Chunks.ContainsKey(ChunksToGenerate[I]) then
    begin
      Chunks.Add(ChunksToGenerate[I], TChunk.Create);
      if GameStart then
      begin
        // do not generate rich or special biomes too close to player's start
        repeat
          Chunks[ChunksToGenerate[I]].BiomeCenterSeed := Xorshift.Random(8) * 2;
        until (Chunks[ChunksToGenerate[I]].BiomeCenterSeed <> BiomeForest) and
              (Chunks[ChunksToGenerate[I]].BiomeCenterSeed <> BiomeCrimson) and
              (Chunks[ChunksToGenerate[I]].BiomeCenterSeed <> BiomeBlue);
        Chunks[ChunksToGenerate[I]].BiomeCenterX := ChunksToGenerate[I].X * ChunkSize + Xorshift.Random(ChunkSize);
        Chunks[ChunksToGenerate[I]].BiomeCenterY := ChunksToGenerate[I].Y * ChunkSize + Xorshift.Random(ChunkSize);
        Chunks[ChunksToGenerate[I]].BiomeRangeMultiplier := 1.0;
      end;
    end;

  repeat
    for I := 0 to Pred(Quantity) do
      GenerateChunk(Chunks[ChunksToGenerate[I]]);
    Finished := true;
    for I := 0 to Pred(Quantity) do
      if Chunks[ChunksToGenerate[I]].AnchorPoints.Count > 0 then
      begin
        Finished := false;
        break;
      end;

    { SUPER RARE (but critical) BUG when somehow we get all the passages blocked out
      Never happened to me, but it is theoretically possible }
    if TotalAnchorPoints < 3 then
    begin
      Finished := false;
      EmergencyDungeonRecovery;
    end;
  until Finished;
end;

procedure TMap.EmergencyDungeonRecovery;
var
  IX, IY, I, K: Integer;
  V: TCoordinateVector;
  VisibleList, InvisibleList: specialize TList<TCoordinateVector>;

  function CanPassSafe(const ABiome: Integer): Boolean;
  begin
    Result := (ABiome = Undefined) or (ABiome = AnchorPoint) or (Biomes[ABiome].CanPass);
  end;

begin
  WriteLnLog('WARNING: The dungeon ended in a stub. Using extreme measures to uncork it. Brace yourselves.');
  VisibleList := specialize TList<TCoordinateVector>.Create;
  InvisibleList := specialize TList<TCoordinateVector>.Create;
  for V in Chunks.Keys do
  begin
    for IX := 1 to ChunkSize - 2 do
      for IY := 1 to ChunkSize - 2 do
        if (Chunks[V].Biome[IX, IY] <> Undefined) and (Chunks[V].Biome[IX, IY] <> AnchorPoint) and
          not Biomes[Chunks[V].Biome[IX, IY]].CanPass then
            if (CanPassSafe(Chunks[V].Biome[IX + 1, IY]) and (Chunks[V].Biome[IX - 1, IY] = Undefined)) or
               (CanPassSafe(Chunks[V].Biome[IX - 1, IY]) and (Chunks[V].Biome[IX + 1, IY] = Undefined)) or
               (CanPassSafe(Chunks[V].Biome[IX, IY + 1]) and (Chunks[V].Biome[IX, IY - 1] = Undefined)) or
               (CanPassSafe(Chunks[V].Biome[IX, IY - 1]) and (Chunks[V].Biome[IX, IY + 1] = Undefined)) then
               begin
                 if Chunks[V].Visible[IX, IY] then
                   VisibleList.Add(CoordinateVector(V.X * ChunkSize + IX, V.Y * ChunkSize + IY))
                 else
                   InvisibleList.Add(CoordinateVector(V.X * ChunkSize + IX, V.Y * ChunkSize + IY));
               end;
  end;
  if InvisibleList.Count >= Chunks.Count then
    for I := 0 to Chunks.Count - 1 do
    begin
      K := Xorshift.Random(InvisibleList.Count);
      AddAnchorPoint(InvisibleList[K].X, InvisibleList[K].Y);
      InvisibleList.Delete(K);
    end
  else
  begin
    WriteLnLog('WARNING TWO: There are not enough invisible points to open up. How could that happen at all?');
    for K := 0 to InvisibleList.Count - 1 do
      AddAnchorPoint(InvisibleList[K].X, InvisibleList[K].Y);
    for I := 0 to Chunks.Count - InvisibleList.Count - 1 do
    begin
      K := Xorshift.Random(VisibleList.Count);
      AddAnchorPoint(VisibleList[K].X, VisibleList[K].Y);
      VisibleList.Delete(K);
    end
  end;
  VisibleList.Free;
  InvisibleList.Free;
end;

function TMap.GetChunkAroundPlayer(const DX, DY: Integer): TChunk;
var
  V: TCoordinateVector;
begin
  V := CoordinateVector(Player.X div ChunkSize + DX, Player.Y div ChunkSize + DY);
  if not Chunks.TryGetValue(V, Result) then
    Exit(nil);
end;

type
  TDelta = record
    DX, DY: ShortInt;
  end;
function Delta(DX, DY: ShortInt): TDelta; inline;
begin
  Result.DX := DX;
  Result.DY := DY;
end;

procedure TMap.GenerateChunk(AChunk: TChunk);
var
  A: Integer;
  ThisBiome: Byte;
  ThisDir: Integer;
  GX, GY: TCoordinate;
  I, IX, IY: Integer;
  Dirs: specialize TList<TDelta>;
  BX, BY: Integer;
  FFFinished: Boolean;
  FloodFill: array[-MaxMaxRoomSize..MaxMaxRoomSize,-MaxMaxRoomSize..MaxMaxRoomSize] of Boolean;

  procedure ProcessAnchors(AX, AY: TCoordinate);
  begin
    if GetBiomeSafe(AX, AY) = Undefined then
    begin
      if Xorshift.Random < Biomes[ThisBiome].WallChance then
        WriteMap(AX, AY, ThisBiome + 1)
      else
        AddAnchorPoint(AX, AY);
    end;
  end;

  procedure WriteWall(AX, AY: TCoordinate);
  begin
    if GetBiomeSafe(AX, AY) = Undefined then
      WriteMap(AX, AY, ThisBiome + 1);
    {if (GetBiomeSafe(AX, AY) = AnchorPoint) and (Xorshift.Random < Biomes[ThisBiome].WallChance) then
      WriteMap(AX, AY, ThisBiome + 1);}
  end;

  procedure WritePass(AX, AY: TCoordinate);
  var
    Enemy: TEnemy;
    Sigil: TSigil;
  begin
    WriteMap(AX, AY, ThisBiome);
    if Xorshift.Random < Biomes[ThisBiome].EnemyChance then
    begin
      Enemy := TEnemy.Create;
      Enemy.X := AX;
      Enemy.Y := AY;
      Enemy.Tile := Xorshift.Random(MaxEnemies);
      //we've just written to this chunk, it must exist for sure
      Chunks[CoordinateVector(Enemy.X div ChunkSize, Enemy.Y div ChunkSize)].Enemies.Add(Enemy);
      Inc(EnemiesStatistics[ThisBiome]);
    end else
    if Xorshift.Random < Biomes[ThisBiome].SigilChance then
    begin
      // Sigil can be placed ONLY between two walls and two passages
      // Note, first we're using a hacky-whacky workaround through Odd, not Biome[].CanPass
      // because Undefined is not in Biome[]
      // but undefined=254 so it's passable
      // Second, the very fact that we're relying on Undefined is not good :)
      if (Odd(GetBiomeSafe(AX + 1, AY)) and Odd(GetBiomeSafe(AX - 1, AY)) and
          not Odd(GetBiomeSafe(AX, AY + 1)) and not Odd(GetBiomeSafe(AX, AY - 1)))
        or
        (not Odd(GetBiomeSafe(AX + 1, AY)) and not Odd(GetBiomeSafe(AX - 1, AY)) and
         Odd(GetBiomeSafe(AX, AY + 1)) and Odd(GetBiomeSafe(AX, AY - 1))) then
      begin
        Sigil := TSigil.Create;
        Sigil.X := AX;
        Sigil.Y := AY;
        Sigil.Tile := Xorshift.Random(MaxSigils);
        //we've just written to this chunk, it must exist for sure
        Chunks[CoordinateVector(Sigil.X div ChunkSize, Sigil.Y div ChunkSize)].Sigils.Add(Sigil);
      end;
    end;
  end;

  procedure WriteTreasure(AX, AY: TCoordinate);
  var
    Treasure: TMapObject;
  begin
    WriteMap(AX, AY, ThisBiome);
    Treasure := TMapObject.Create;
    Treasure.X := AX;
    Treasure.Y := AY;
    Treasure.Tile := Xorshift.Random(6);
    //we've just written to this chunk, it must exist for sure
    Chunks[CoordinateVector(AX div ChunkSize, AY div ChunkSize)].Treasure.Add(Treasure);
    Inc(TreasureStatistics[ThisBiome]);
  end;

  procedure GenerateRoom(RoomStartX, RoomStartY: TCoordinate; RoomSizeX, RoomSizeY: Integer);
  var
    JX, JY: Integer;
    TempBiome: Byte;
    Treasure: TMapObject;
  begin
    if (RoomSizeX = 0) or (RoomSizeY = 0) then
      raise Exception.Create('(RoomSizeX = 0) or (RoomSizeY = 0)');
    for JX := -RoomSizeX to RoomSizeX do
      for JY := -RoomSizeY to RoomSizeY do
      begin
        TempBiome := GetBiomeSafe(RoomStartX + JX, RoomStartY + JY);
        if (TempBiome <> Undefined) and (TempBiome <> AnchorPoint) then
          if ((Abs(JX) <> RoomSizeX) and (Abs(JY) <> RoomSizeY)) or Biomes[TempBiome].CanPass then
            Exit;
      end;
    for JX := -RoomSizeX to RoomSizeX do
      for JY := -RoomSizeY to RoomSizeY do
        if (Abs(JX) = RoomSizeX) and (Abs(JY) = RoomSizeY) then
          continue
        else
        if (Abs(JX) = RoomSizeX) or (Abs(JY) = RoomSizeY) then
          WriteWall(RoomStartX + JX, RoomStartY + JY)
        else
          if (JX <> 0) or (JY <> 0) then
            WritePass(RoomStartX + JX, RoomStartY + JY)
          else
            WriteMap(RoomStartX + JX, RoomStartY + JY, ThisBiome);
    Treasure := TMapObject.Create;
    Treasure.X := RoomStartX;
    Treasure.Y := RoomStartY;
    Treasure.Tile := Xorshift.Random(6);
    //we've just written to this chunk, it must exist for sure
    Chunks[CoordinateVector(Treasure.X div ChunkSize, Treasure.Y div ChunkSize)].Treasure.Add(Treasure);
    Inc(TreasureStatistics[ThisBiome]);
  end;

  function GenerateStructure(RoomStartX, RoomStartY: TCoordinate): Boolean;
  var
    JX, JY: Integer;
    TempBiome: Byte;
    Struct: TStructure;
    ThisAnchor: Integer;
  begin
    Struct := Structures[Xorshift.Random(Structures.Count)];
    ThisAnchor := Xorshift.Random(Length(Struct.Anchors));

    // test if can place room
    for JY := Low(Struct.Layout) to High(Struct.Layout) do
      for JX := Low(Struct.Layout[JY]) to High(Struct.Layout[JY]) do
      if Struct.Layout[JY, JX] > 0 then
      begin
        TempBiome := GetBiomeSafe(RoomStartX + JX - Struct.Anchors[ThisAnchor].X, RoomStartY + JY - Struct.Anchors[ThisAnchor].Y);
        if (TempBiome <> Undefined) and (TempBiome <> AnchorPoint) then
          if Biomes[TempBiome].CanPass then
            Exit(false);
      end;

    case Xorshift.Random(3) of // it'll be reset on next iteration, so safe to change here
      0: ThisBiome := BiomeCrystal;
      1: ThisBiome := BiomeMarble;
      2: ThisBiome := BiomeLab;
    end;
    for JY := Low(Struct.Layout) to High(Struct.Layout) do
      for JX := Low(Struct.Layout[JY]) to High(Struct.Layout[JY]) do
        case Struct.Layout[JY, JX] of
          //0: continue;
          1: WritePass(RoomStartX + JX - Struct.Anchors[ThisAnchor].X, RoomStartY + JY - Struct.Anchors[ThisAnchor].Y);
          2: WriteWall(RoomStartX + JX - Struct.Anchors[ThisAnchor].X, RoomStartY + JY - Struct.Anchors[ThisAnchor].Y);
          3: WriteTreasure(RoomStartX + JX - Struct.Anchors[ThisAnchor].X, RoomStartY + JY - Struct.Anchors[ThisAnchor].Y);
          4: if GetBiomeSafe(RoomStartX + JX - Struct.Anchors[ThisAnchor].X, RoomStartY + JY - Struct.Anchors[ThisAnchor].Y) = Undefined then
               AddAnchorPoint(RoomStartX + JX - Struct.Anchors[ThisAnchor].X, RoomStartY + JY - Struct.Anchors[ThisAnchor].Y);
        end;
    Result := true;
  end;

var
  Count: Integer;
begin
  Dirs := (specialize TList<TDelta>).Create;
  while AChunk.AnchorPoints.Count > 0 do
  begin
    A := Xorshift.Random(AChunk.AnchorPoints.Count);
    //WriteLnLog(A.ToString + ' ' + AChunk.AnchorPoints.Count.ToString);
    Dirs.Clear;
    if GetBiomeSafe(Achunk.AnchorPoints[A].X + 1, Achunk.AnchorPoints[A].Y) = Undefined then
      Dirs.Add(Delta(+1, 0));
    if GetBiomeSafe(Achunk.AnchorPoints[A].X - 1, Achunk.AnchorPoints[A].Y) = Undefined then
      Dirs.Add(Delta(-1, 0));
    if GetBiomeSafe(Achunk.AnchorPoints[A].X, Achunk.AnchorPoints[A].Y + 1) = Undefined then
      Dirs.Add(Delta(0, +1));
    if GetBiomeSafe(Achunk.AnchorPoints[A].X, Achunk.AnchorPoints[A].Y - 1) = Undefined then
      Dirs.Add(Delta(0, -1));
    if Dirs.Count = 0 then
      WriteMap(Achunk.AnchorPoints[A].X, Achunk.AnchorPoints[A].Y, GetChunkBiome(Achunk.AnchorPoints[A].X, Achunk.AnchorPoints[A].Y))  //this will also remove the AnchorPoint
    else
    begin
      ThisDir := Xorshift.Random(Dirs.Count);
      GX := Achunk.AnchorPoints[A].X;
      GY := Achunk.AnchorPoints[A].Y;
      ThisBiome := GetChunkBiome(GX, GY);
      if Xorshift.Random < Biomes[ThisBiome].StructureChance then
      begin
        Count := 0;
        while (not GenerateStructure(GX, GY)) and (Count < Structures.Count * 2) do
          Inc(Count);
      end else
      if Xorshift.Random < Biomes[ThisBiome].RoomChance then
      begin
        IX := Xorshift.Random(Biomes[ThisBiome].MaxRoomSize - Biomes[ThisBiome].MinRoomSize + 1) + Biomes[ThisBiome].MinRoomSize;
        IY := Xorshift.Random(Biomes[ThisBiome].MaxRoomSize - Biomes[ThisBiome].MinRoomSize + 1) + Biomes[ThisBiome].MinRoomSize;
        if Dirs[ThisDir].DX <> 0 then
          GenerateRoom(GX + Dirs[ThisDir].DX * IX, GY, IX, IY)
        else
          GenerateRoom(GX, GY + Dirs[ThisDir].DY * IY, IX, IY);
      end else
        case Biomes[ThisBiome].Algorithm of
          baPass, baStrictPass, baHedgehog:
            begin
              WritePass(GX, GY); //this will also remove the AnchorPoint
              if Dirs[ThisDir].DX <> 0 then
              begin
                WriteWall(GX, GY + 1);
                WriteWall(GX, GY - 1);
                WriteWall(GX - Dirs[ThisDir].DX, GY);
              end else
              begin
                WriteWall(GX + 1, GY);
                WriteWall(GX - 1, GY);
                WriteWall(GX, GY - Dirs[ThisDir].DY);
              end;
              for I := Xorshift.Random(Biomes[ThisBiome].MaxSize - Biomes[ThisBiome].MinSize + 1) + Biomes[ThisBiome].MinSize downto 0 do
              begin
                GX += Dirs[ThisDir].DX;
                GY += Dirs[ThisDir].DY;
                if (GetBiomeSafe(GX, GY) <> Undefined) then
                  break; // this is a deadend, stop here and forget about this
                if I > 0 then
                begin
                  WritePass(GX, GY);
                  if Dirs[ThisDir].DX <> 0 then
                  begin
                    if (Biomes[ThisBiome].Algorithm = baHedgehog) and Odd(GX) and (GetBiomeSafe(GX, GY + 1) = undefined) and (GetBiomeSafe(GX, GY - 1) = undefined) then
                    begin
                      WritePass(GX, GY + 1);
                      WritePass(GX, GY - 1);
                      WriteWall(GX, GY + 2);
                      WriteWall(GX, GY - 2);
                      WriteWall(GX + 1, GY + 1);
                      WriteWall(GX - 1, GY + 1);
                      WriteWall(GX + 1, GY - 1);
                      WriteWall(GX - 1, GY - 1);
                    end else
                    begin
                      WriteWall(GX, GY + 1);
                      WriteWall(GX, GY - 1);
                    end;
                  end else
                  begin
                    if (Biomes[ThisBiome].Algorithm = baHedgehog) and Odd(GY) and (GetBiomeSafe(GX + 1, GY) = undefined) and (GetBiomeSafe(GX - 1, GY) = undefined) then
                    begin
                      WritePass(GX + 1, GY);
                      WritePass(GX - 1, GY);
                      WriteWall(GX + 2, GY);
                      WriteWall(GX - 2, GY);
                      WriteWall(GX + 1, GY + 1);
                      WriteWall(GX - 1, GY + 1);
                      WriteWall(GX + 1, GY - 1);
                      WriteWall(GX - 1, GY - 1);
                    end else
                    begin
                      WriteWall(GX + 1, GY);
                      WriteWall(GX - 1, GY);
                    end;
                  end;
                end else
                begin
                  WritePass(GX, GY);
                  ProcessAnchors(GX + 1, GY);
                  ProcessAnchors(GX - 1, GY);
                  ProcessAnchors(GX, GY + 1);
                  ProcessAnchors(GX, GY - 1);
                  if (Biomes[ThisBiome].Algorithm = baStrictPass) then
                  begin
                    WriteWall(GX + 1, GY + 1);
                    WriteWall(GX - 1, GY + 1);
                    WriteWall(GX - 1, GY - 1);
                    WriteWall(GX + 1, GY - 1);
                  end;
                end;
              end;
            end;
          baBox:
            begin
              BX := Xorshift.Random(Biomes[ThisBiome].MaxSize - Biomes[ThisBiome].MinSize + 1) + Biomes[ThisBiome].MinSize;
              BY := Xorshift.Random(Biomes[ThisBiome].MaxSize - Biomes[ThisBiome].MinSize + 1) + Biomes[ThisBiome].MinSize;
              for IX := -BX - 2 to BX + 2 do
                for IY := -BY - 2 to BY + 2 do
                  FloodFill[IX, IY] := false;
              FloodFill[0, 0] := true;
              WritePass(GX, GY);
              repeat
                FFFinished := true;
                for IX := -BX - 1 to BX + 1 do
                  for IY := -BY - 1 to BY + 1 do
                    if (Abs(IX) = BX + 1) and (Abs(IY) = BY + 1) then
                      continue
                    else
                    if not FloodFill[IX, IY] then
                      if FloodFill[IX + 1, IY] or FloodFill[IX - 1, IY] or
                        FloodFill[IX, IY + 1] or FloodFill[IX, IY - 1] then
                          if (Abs(IX) = BX + 1) or (Abs(IY) = BY + 1) then
                          begin
                            ProcessAnchors(GX + IX, GY + IY);
                            if GetBiomeSafe(GX + IX, GY + IY) = AnchorPoint then
                            begin
                              FFFinished := false;
                              FloodFill[IX, IY] := true;
                            end;
                          end else
                          if GetBiomeSafe(GX + IX, GY + IY) = Undefined then
                          begin
                            FFFinished := false;
                            FloodFill[IX, IY] := true;
                            WritePass(GX + IX, GY + IY);
                          end;
              until FFFinished;
              {for IX := -BX - 1 to BX + 1 do
                for IY := -BY - 1 to BY + 1 do
                  if (Abs(IX) = BX + 1) and (Abs(IY) = BY + 1) then
                    continue
                  else
                  if (Abs(IX) = BX + 1) or (Abs(IY) = BY + 1) then
                    ProcessAnchors(GX + IX, GY + IY)
                  else
                  if (GetBiomeSafe(GX + IX, GY + IY) = Undefined) then
                    WritePass(GX + IX, GY + IY)
                  else
                  if (IX = 0) and (IY = 0) then
                    WritePass(GX + IX, GY + IY);  }
            end;
        end;
    end;
  end;
  Dirs.Free;
end;

function TMap.GetBiomeSafe(const X, Y: TCoordinate): Byte;
var
  V: TCoordinateVector;
  CX, CY: Integer;
begin
  V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
  if not Chunks.ContainsKey(V) then
    Exit(Undefined);
  CX := X mod ChunkSize;
  CY := Y mod ChunkSize;
  Result := Chunks[V].Biome[CX, CY];
end;

function TMap.GetBiome(const X, Y: TCoordinate): Byte;
var
  V: TCoordinateVector;
  CX, CY: Integer;
begin
  V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
  CX := X mod ChunkSize;
  CY := Y mod ChunkSize;
  if not Chunks.ContainsKey(V) then
    Chunks.Add(V, TChunk.Create); // TEMPORARY
  Result := Chunks[V].Biome[CX, CY];
end;

function TMap.GetTile(const X, Y: TCoordinate): Byte;
var
  V: TCoordinateVector;
  CX, CY: Integer;
begin
  V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
  CX := X mod ChunkSize;
  CY := Y mod ChunkSize;
  if not Chunks.ContainsKey(V) then
    Chunks.Add(V, TChunk.Create); // TEMPORARY
  Result := Chunks[V].Tile[CX, CY];
end;

function TMap.CanPass(const X, Y: TCoordinate): Boolean;
var
  V: TCoordinateVector;
  CX, CY: Integer;
begin
  V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
  CX := X mod ChunkSize;
  CY := Y mod ChunkSize;
  if not Chunks.ContainsKey(V) then
    Exit(false);
  if (Chunks[V].Biome[CX, CY] = Undefined) or (Chunks[V].Biome[CX, CY] = AnchorPoint) then
    Exit(false);

  Result := Biomes[Chunks[V].Biome[CX, CY]].CanPass;
end;

function TMap.CanPassEnemy(const X, Y: TCoordinate): Boolean;
var
  MapObject: TMapObject;
  V: TCoordinateVector;
begin
  Result := false;
  if CanPass(X, Y) then
  begin
    Result := true;
    V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
    for MapObject in Chunks[V].Sigils do
      if (MapObject.X = X) and (MapObject.Y = Y) then
        Exit(false);
    for MapObject in Chunks[V].Enemies do
      if (MapObject.X = X) and (MapObject.Y = Y) then
        Exit(false);
    {for MapObject in Chunks[V].Treasure do
      if (MapObject.X = X) and (MapObject.Y = Y) then
        Exit(false);}
  end;
end;

function TMap.CanTeleportHere(const X, Y: TCoordinate): Boolean;
var
  MapObject: TMapObject;
  V: TCoordinateVector;
begin
  Result := CanPassEnemy(X, Y);
  if Result then
  begin
    V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
    for MapObject in Chunks[V].Treasure do
      if (MapObject.X = X) and (MapObject.Y = Y) then
        Exit(false);
  end;
end;

function TMap.CanPassPlayer(const X, Y: TCoordinate): Boolean;
begin
  Result := CanPass(X, Y) and GetVisible(X, Y);
end;

function TMap.GetVisible(const X, Y: TCoordinate): Boolean;
var
  V: TCoordinateVector;
  CX, CY: Integer;
begin
  V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
  CX := X mod ChunkSize;
  CY := Y mod ChunkSize;
  if not Chunks.ContainsKey(V) then
    Exit(false);
  if Chunks[V].Tile[CX, CY] = Undefined then
    Exit(false);          // TEMPORARY
  if Chunks[V].Tile[CX, CY] = AnchorPoint then
    Exit(true);          // TEMPORARY
  Result := Chunks[V].Visible[CX, CY];
end;

function TMap.FindNearestGoToDestination(const AVector: TCoordinateVector): TCoordinateVector;
var
  I, D: Integer;
begin
  I := 0;
  repeat
    Inc(I);
    for D := -I to I do
      if CanPassPlayer(AVector.X + D, AVector.Y + I) then
        Exit(CoordinateVector(AVector.X + D, AVector.Y + I));
    for D := -I to I do
      if CanPassPlayer(AVector.X + D, AVector.Y - I) then
        Exit(CoordinateVector(AVector.X + D, AVector.Y - I));
    for D := -I to I do
      if CanPassPlayer(AVector.X + I, AVector.Y + D) then
        Exit(CoordinateVector(AVector.X + I, AVector.Y + D));
    for D := -I to I do
      if CanPassPlayer(AVector.X - I, AVector.Y + D) then
        Exit(CoordinateVector(AVector.X - I, AVector.Y + D));
  until I > ChunkSize * 4; // some sanity-check fallback
  Result := AVector;
end;

procedure TMap.SetVisible(const X, Y: TCoordinate);
var
  V: TCoordinateVector;
  CX, CY: Integer;
begin
  V := CoordinateVector(X div ChunkSize, Y div ChunkSize);
  CX := X mod ChunkSize;
  CY := Y mod ChunkSize;
  if not Chunks.ContainsKey(V) then
    Chunks.Add(V, TChunk.Create);
  Chunks[V].Visible[CX, CY] := true;
end;

procedure TMap.DebugSetAllVisible;
var
  C: TChunk;
  CX, CY: Integer;
begin
  for C in Chunks.Values do
    for CX := 0 to Pred(ChunkSize) do
      for CY := 0 to Pred(ChunkSize) do
        C.Visible[CX, CY] := true;
end;

end.

