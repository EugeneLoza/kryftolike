{  Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAchievements;

interface

var
  OnNewAchievement: procedure (const AchievementUrl: String) of object;

procedure SetAchievement(const AName: String);
function HasAchievement(const AName: String): Boolean;
implementation
uses
  CastleConfig, CastleLog,
  GameSounds;

procedure SetAchievement(const AName: String);
begin
  if not HasAchievement(AName) then
  begin
    Sound('achievement');
    WriteLnLog('Unlocked achievement: ' + AName);
    UserConfig.SetValue('ach_' + AName, true);
    UserConfig.Save;
    if Assigned(OnNewAchievement) then
      OnNewAchievement('castle-data:/achievements/' + AName + '.castle-user-interface');
  end;
end;

function HasAchievement(const AName: String): Boolean;
begin
  Result := UserConfig.GetValue('ach_' + AName, false);
end;

end.



