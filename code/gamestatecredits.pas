{  Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStateCredits;

interface

uses
  Classes, SysUtils, CastleUiControls, CastleKeysMouse;

type
  TStateCredits = class(TCastleView)
  public
    constructor Create(AOwner: TComponent); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  StateCredits: TStateCredits;

implementation
uses
  CastleComponentSerialize,
  GameStateOptions, GameSounds;

constructor TStateCredits.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/credits.castle-user-interface';
  DesignPreload := false;
end;

function TStateCredits.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  Sound('ui_click');
  Container.View := StateOptions;
end;

end.

