{  Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSounds;

interface

procedure Sound(const SoundName: String);
procedure Music(const SoundName: String);
procedure InitializeSounds(const Url: String);
implementation
uses
  SysUtils, Classes, Generics.Collections, DOM,
  CastleSoundEngine, CastleXmlUtils, CastleLog, CastleUriUtils;

type
  TSoundContainer = class(TCastleSound) // cannot call it TSound because of deprecated TSound in CastleEngine which confuses FPC
  public
    SoundName: String;
    class function Read(const BaseUrl: String; Element: TDOMElement): TSoundContainer;
  end;
  TSoundsDictionary = specialize TObjectDictionary<String, TSoundContainer>;

class function TSoundContainer.Read(const BaseUrl: String; Element: TDOMElement): TSoundContainer;
begin
  // crop of TRepoSoundEngine.TSoundInfoBuffer.ReadElement
  Result := TSoundContainer.Create(nil);
  Result.SoundName := Element.AttributeString('Name'); //  Note: not Result.Name, because latter one is component name and adds a lot of (weird) requirements on top
  Result.Volume := Element.AttributeSingleDef('Volume', 1);
  Result.Priority := Element.AttributeSingleDef('Priority', 0.5);
  Result.Stream := Element.AttributeBooleanDef('Stream', false);
  { set URL at the end, to avoid reloading when Sound.Stream changes }
  Result.Url := CombineURI(BaseUrl, Element.AttributeString('Url'));
end;

var
  SoundsDictionary: TSoundsDictionary;

function GetSoundByName(const SoundName: String): TSoundContainer; inline;
var
  S: TSoundContainer;
begin
  if SoundsDictionary.TryGetValue(SoundName, S) then
    Exit(S)
  else
  begin
    WriteLnWarning('Sound not found: %s', [SoundName]);
    Exit(nil);
  end;
end;

procedure Sound(const SoundName: String);
begin
  SoundEngine.Play(GetSoundByName(SoundName));
end;

procedure Music(const SoundName: String);
begin
  SoundEngine.LoopingChannel[0].Sound := GetSoundByName(SoundName);
end;

procedure InitializeSounds(const Url: String);
var
  Doc: TXMLDocument;
  Sound: TSoundContainer;
  Iterator: TXMLElementIterator;
begin
  Doc := URLReadXML(Url);
  SoundsDictionary := TSoundsDictionary.Create([doOwnsValues]);

  Iterator := Doc.DocumentElement.ChildrenIterator('Sound');
  try
    while Iterator.GetNext do
    begin
      Sound := TSoundContainer.Read(ExtractUriPath(Url), Iterator.Current);
      SoundsDictionary.Add(Sound.SoundName, Sound);
    end;
  finally
    FreeAndNil(Iterator);
  end;
  FreeAndNil(Doc);
end;


finalization
  FreeAndNil(SoundsDictionary);

end.

