{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Predesigned structure on the map }
unit GameMapStructures;

{$mode objfpc}{$H+}

interface
uses
  Generics.Collections;

type
  TAnchorPoint = record
    X, Y: Integer;
  end;
  TStructure = class(TObject)
    Width, Height: Integer;
    Anchors: array of TAnchorPoint;
    {
      // 0 = undefined
      // 1 = pass
      // 2 = wall
      // 3 = treasure
      // 4 = anchor
    }
    Layout: array of array of Byte;

    function FlipY: TStructure;
    function FlipX: TStructure;
    function FlipXY: TStructure;
    function Transpose: TStructure;
    function TransposeFlipY: TStructure;
    function TransposeFlipX: TStructure;
    function TransposeFlipXY: TStructure;
  end;
  TStructuresList = specialize TObjectList<TStructure>;

var
  Structures: TStructuresList;

procedure LoadStructures;
implementation
uses
  SysUtils, Classes,
  CastleFindFiles, CastleDownload, CastleLog;

procedure FoundStructure(const FileInfo: TFileInfo; Data: Pointer;
  var StopSearch: Boolean);
var
  Stream: TStream;
  Strings: TStringList;
  Structure: TStructure;
  I, J, K: Integer;
begin
  Stream := Download(FileInfo.Url);
  Strings := TStringList.Create;
  Strings.LoadFromStream(Stream);
  FreeAndNil(Stream);

  Structure := TStructure.Create;
  Structure.Width := -1;
  for K := 0 to Pred (Strings.Count) do
    if Length(Strings[K]) > 0 then // or first symbol is "comment"
    begin
      SetLength(Structure.Layout, Length(Structure.Layout) + 1);
      J := Pred(Length(Structure.Layout));
      SetLength(Structure.Layout[J], Length(Strings[K]));

      if Structure.Width < 0 then
        Structure.Width := Length(Strings[K])
      else
      if Structure.Width <> Length(Strings[K]) then
        WritelnWarning('Structure width inconsistent: %d <> %d', [Structure.Width, Length(Strings[K])]);

      for I := 1 to Length(Strings[K]) do
        case Strings[K][I] of
          '-': Structure.Layout[J][Pred(I)] := 0; // undefined
          '.': Structure.Layout[J][Pred(I)] := 1; // pass
          '#': Structure.Layout[J][Pred(I)] := 2; // wall
          '$': Structure.Layout[J][Pred(I)] := 3; // treasure
          '+':
            begin
              Structure.Layout[J][Pred(I)] := 4; // anchor
              SetLength(Structure.Anchors, Length(Structure.Anchors) + 1);
              Structure.Anchors[Pred(Length(Structure.Anchors))].X := Pred(I);
              Structure.Anchors[Pred(Length(Structure.Anchors))].Y := J;
            end;
          else
            WriteLnWarning('Unexpected symbol in %s at line %d symbol %d', [FileInfo.Url, K, I])
        end;
    end;
  Structure.Height := Length(Structure.Layout);
  if Length(Structure.Anchors) = 0 then
  begin
    FreeAndNil(Structure);
    WriteLnWarning('Structure %s doesn''t have anchors', [FileInfo.Url]);
    Exit;
  end;

  Structures.Add(Structure);
  Structures.Add(Structure.FlipY);
  Structures.Add(Structure.FlipX);
  Structures.Add(Structure.FlipXY);
  Structures.Add(Structure.Transpose);
  Structures.Add(Structure.TransposeFlipX);
  Structures.Add(Structure.TransposeFlipY);
  Structures.Add(Structure.TransposeFlipXY);

  FreeAndNil(Strings);
end;

procedure LoadStructures;
begin
  Structures := TStructuresList.Create(true);
  FindFiles('castle-data:/structures/*.txt', false, @FoundStructure, nil, []);
  WriteLnLog('Structures loaded (8x): %d', [Structures.Count]);
end;

function TStructure.FlipY: TStructure;
var
  I: Integer;
begin
  Result := TStructure.Create;
  Result.Width := Width;
  Result.Height := Height;
  SetLength(Result.Layout, Result.Height);
  for I := 0 to Pred(Result.Height) do
    Result.Layout[I] := Layout[Pred(Result.Height) - I];
  SetLength(Result.Anchors, Length(Anchors));
  for I := 0 to Pred(Length(Anchors)) do
  begin
    Result.Anchors[I].X := Anchors[I].X;
    Result.Anchors[I].Y := Pred(Result.Height) - Anchors[I].Y;
  end;
end;

function TStructure.FlipX: TStructure;
var
  I, J: Integer;
begin
  Result := TStructure.Create;
  Result.Width := Width;
  Result.Height := Height;
  SetLength(Result.Layout, Result.Height);
  for I := 0 to Pred(Result.Height) do
  begin
    SetLength(Result.Layout[I], Result.Width);
    for J := 0 to Pred(Result.Width) do
      Result.Layout[I][J] := Layout[I][Pred(Result.Width) - J];
  end;
  SetLength(Result.Anchors, Length(Anchors));
  for I := 0 to Pred(Length(Anchors)) do
  begin
    Result.Anchors[I].X := Pred(Result.Width) - Anchors[I].X;
    Result.Anchors[I].Y := Anchors[I].Y;
  end;
end;

function TStructure.FlipXY: TStructure;
var
  I, J: Integer;
begin
  Result := TStructure.Create;
  Result.Width := Width;
  Result.Height := Height;
  SetLength(Result.Layout, Result.Height);
  for I := 0 to Pred(Result.Height) do
  begin
    SetLength(Result.Layout[I], Result.Width);
    for J := 0 to Pred(Result.Width) do
      Result.Layout[I][J] := Layout[Pred(Height) - I][Pred(Width) - J];
  end;
  SetLength(Result.Anchors, Length(Anchors));
  for I := 0 to Pred(Length(Anchors)) do
  begin
    Result.Anchors[I].X := Pred(Result.Width) - Anchors[I].X;
    Result.Anchors[I].Y := Pred(Result.Height) - Anchors[I].Y;
  end;
end;

function TStructure.Transpose: TStructure;
var
  I, J: Integer;
begin
  Result := TStructure.Create;
  Result.Width := Height;
  Result.Height := Width;
  SetLength(Result.Layout, Result.Height);
  for I := 0 to Pred(Result.Height) do
  begin
    SetLength(Result.Layout[I], Result.Width);
    for J := 0 to Pred(Result.Width) do
      Result.Layout[I][J] := Layout[J][I];
  end;
  SetLength(Result.Anchors, Length(Anchors));
  for I := 0 to Pred(Length(Anchors)) do
  begin
    Result.Anchors[I].X := Anchors[I].Y;
    Result.Anchors[I].Y := Anchors[I].X;
  end;
end;

function TStructure.TransposeFlipY: TStructure;
var
  I, J: Integer;
begin
  Result := TStructure.Create;
  Result.Width := Height;
  Result.Height := Width;
  SetLength(Result.Layout, Result.Height);
  for I := 0 to Pred(Result.Height) do
  begin
    SetLength(Result.Layout[I], Result.Width);
    for J := 0 to Pred(Result.Width) do
      Result.Layout[I][J] := Layout[Pred(Height) - J][I];
  end;
  SetLength(Result.Anchors, Length(Anchors));
  for I := 0 to Pred(Length(Anchors)) do
  begin
    Result.Anchors[I].X := Pred(Result.Width) - Anchors[I].Y;
    Result.Anchors[I].Y := Anchors[I].X;
  end;
end;

function TStructure.TransposeFlipX: TStructure;
var
  I, J: Integer;
begin
  Result := TStructure.Create;
  Result.Width := Height;
  Result.Height := Width;
  SetLength(Result.Layout, Result.Height);
  for I := 0 to Pred(Result.Height) do
  begin
    SetLength(Result.Layout[I], Result.Width);
    for J := 0 to Pred(Result.Width) do
      Result.Layout[I][J] := Layout[J][Pred(Width) - I];
  end;
  SetLength(Result.Anchors, Length(Anchors));
  for I := 0 to Pred(Length(Anchors)) do
  begin
    Result.Anchors[I].X := Pred(Result.Width) - Anchors[I].Y;
    Result.Anchors[I].Y := Anchors[I].X;
  end;
end;

function TStructure.TransposeFlipXY: TStructure;
var
  I, J: Integer;
begin
  Result := TStructure.Create;
  Result.Width := Height;
  Result.Height := Width;
  SetLength(Result.Layout, Result.Height);
  for I := 0 to Pred(Result.Height) do
  begin
    SetLength(Result.Layout[I], Result.Width);
    for J := 0 to Pred(Result.Width) do
      Result.Layout[I][J] := Layout[Pred(Height) - J][Pred(Width) - I];
  end;
  SetLength(Result.Anchors, Length(Anchors));
  for I := 0 to Pred(Length(Anchors)) do
  begin
    Result.Anchors[I].X := Pred(Result.Width) - Anchors[I].Y;
    Result.Anchors[I].Y := Pred(Result.Height) - Anchors[I].X;
  end;
end;

finalization
  FreeAndNil(Structures);
end.

