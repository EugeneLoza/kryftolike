{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit Global;

{$mode objfpc}{$H+}

interface

uses
  CastleRandom, CastleWindow;

type
  TSignedCoordinate = Int64;
  TCoordinate = Int64; //UInt64 / 2

  type
    TCoordinateVector = record
      X, Y: TCoordinate;
    end;

var
  Xorshift: TCastleRandom;
  WindowScale: Single;
  Window: TCastleWindow;

function CoordinateVector(X, Y: TCoordinate): TCoordinateVector; inline;
implementation

function CoordinateVector(X, Y: TCoordinate): TCoordinateVector; inline;
begin
  Result.X := X;
  Result.Y := Y;
end;

initialization
  Xorshift := TCastleRandom.Create;
finalization
  Xorshift.Free;
end.

