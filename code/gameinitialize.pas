{ Copyright (C) 2021-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameInitialize;

interface

procedure SetFullScreen(const AFullScreen: Boolean);
implementation

uses SysUtils, Classes,
  CastleWindow, CastleLog,
  CastleKeysMouse, CastleColors,
  CastleUIControls, CastleApplicationProperties,
  CastleSoundEngine, CastleConfig,
  BatchedMap, Global, GameMap, GameHints, GameSplash, GameSave, GameSounds,
  GameMapStructures
  {$region 'Castle Initialization Uses'}
  // The content here may be automatically updated by CGE editor.
  , GameStateMain
  , GameStateMainMenu
  , GameStateInGameMenu
  , GameStateAchievements
  , GameStateOptions
  , GameStateCredits
  , GameViewHowToPlay
  {$endregion 'Castle Initialization Uses'};

procedure ApplicationInitialize;
begin
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');

  UserConfig.Load;
  SetFullScreen(UserConfig.GetValue('fullscreen', false));

  LoadStructures;
  InitializeSounds('castle-data:/audio/index.xml');
  SoundEngine.Volume := UserConfig.GetFloat('volume', 1.0);
  SoundEngine.LoopingChannel[0].Volume := UserConfig.GetFloat('music', 1.0);

  LoadHints;
  LoadTileset;

  {$region 'Castle State Creation'}
  // The content here may be automatically updated by CGE editor.
  StateAchievements := TStateAchievements.Create(Application);
  StateMain := TStateMain.Create(Application);
  StateMainMenu := TStateMainMenu.Create(Application);
  StateInGameMenu := TStateInGameMenu.Create(Application);
  StateOptions := TStateOptions.Create(Application);
  StateCredits := TStateCredits.Create(Application);
  ViewHowToPlay := TViewHowToPlay.Create(Application);;
  {$endregion 'Castle State Creation'}

  Window.Container.View := StateMainMenu;
end;

procedure SetFullScreen(const AFullScreen: Boolean);
begin
  Window.FullScreen := AFullScreen;
end;

procedure WindowPress(Container: TUIContainer; const Event: TInputPressRelease);
begin
  if Event.Key = keyF11 then
  begin
    SetFullScreen(not Window.FullScreen);
    UserConfig.SetValue('fullscreen', Window.FullScreen);
    UserConfig.Save;
  end;
  if Event.Key = keyF5 then
    Container.SaveScreen('screenshot_kryftolike' + IntToStr(Round(Now * 24 * 60 * 60 * 100)) + '.png');
end;

procedure WindowClose(Container: TUIContainer);
begin
  {$IFNDEF Android}
  SaveGame;
  {$ENDIF}
  Window.Close;
end;

initialization
  if IsLibrary then
    InitializeLog;

  WriteLnLog('----------------------------------------------------');
  WriteLnLog(ApplicationProperties.Caption + ' ' + ApplicationProperties.Version);
  WriteLnLog('Copyright (C) 2021-2024 Yevhen Loza');
  WriteLnLog('This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.');
  WriteLnLog('This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.');
  WriteLnLog('You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.');
  WriteLnLog('----------------------------------------------------');

  Theme.LoadingBackgroundColor := Black;
  Theme.ImagesPersistent[tiLoading].Image := Splash;
  Theme.ImagesPersistent[tiLoading].OwnsImage := false;
  Theme.ImagesPersistent[tiLoading].SmoothScaling := false;
  Theme.LoadingUIScaling := usEncloseReferenceSize;
  Theme.LoadingUIReferenceWidth := Splash.Width;
  Theme.LoadingUIReferenceHeight := Splash.Height;

  Application.OnInitialize := @ApplicationInitialize;

  Window := TCastleWindow.Create(Application);
  {$IFNDEF Android}
    Window.Height := Application.ScreenHeight * 4 div 5;
    Window.Width := Window.Height * 1334 div 750;
    WindowScale := Window.Height / 750.0;
    //Window.FpsShowOnCaption := true;
    Window.OnPress := @WindowPress;
  {$ENDIF}
  Window.AutoRedisplay := false;
  Window.Container.BackgroundColor := Black;
  //Window.Container.BackgroundEnable := false;
  Window.OnCloseQuery := @WindowClose;
  Application.MainWindow := Window;
end.
