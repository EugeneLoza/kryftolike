#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

#!/bin/bash
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$script_path"

version="1.10."$(git rev-list --count HEAD)
echo \'$version\' >code/version.inc
echo $version
