#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

find -type f -name '*.pas' -prune -exec unix2dos {} \;
find -type f -name '*.inc' -prune -exec unix2dos {} \;
