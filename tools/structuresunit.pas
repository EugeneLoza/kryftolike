unit StructuresUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

const
  MaxSize = 24;

type
  TMapItem = class(TPanel)
  public
    MX, MY: Integer;
  end;

type
  TForm1 = class(TForm)
    ButtonClearMap: TButton;
    PalettePass: TPanel;
    PaletteAnchor: TPanel;
    PaletteTreasure: TPanel;
    PaletteUndefined: TPanel;
    Map: TPanel;
    PaletteWall: TPanel;
    SizeX: TEdit;
    FileName: TEdit;
    SaveFile: TButton;
    SizeY: TEdit;
    procedure ButtonClearMapClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure PaletteAnchorClick(Sender: TObject);
    procedure PalettePassClick(Sender: TObject);
    procedure PaletteTreasureClick(Sender: TObject);
    procedure PaletteUndefinedClick(Sender: TObject);
    procedure PaletteWallClick(Sender: TObject);
    procedure SaveFileClick(Sender: TObject);
    procedure SizeXEditingDone(Sender: TObject);
    procedure SizeYEditingDone(Sender: TObject);
  private
    W: Integer;
    H: Integer;
    M: array[0..MaxSize-1, 0..MaxSize-1] of Char;
    B: array[0..MaxSize-1, 0..MaxSize-1] of TMapItem;
    procedure ClickBox(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    function GetActiveChar: Char;
    function GetColor(const C: Char): TColor;
    procedure DrawMap;
    procedure ClearMap;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

type
  TPanelHelper = class helper for TPanel
  private
    function GetChecked: Boolean;
    procedure SetChecked(AValue: Boolean);
  public
    property Checked: Boolean read GetChecked write SetChecked;
  end;

function TPanelHelper.GetChecked: Boolean;
begin
  Exit(BevelWidth < 7);
end;

procedure TPanelHelper.SetChecked(AValue: Boolean);
begin
  if AValue then
    BevelWidth := 1
  else
    BevelWidth := 7;
end;

procedure TForm1.SaveFileClick(Sender: TObject);
var
  F: Text;
  IX, IY: Integer;
  S: String;
begin
  AssignFile(F, FileName.Text + '.txt');
  Rewrite(F);
  for IY := 0 to Pred(H) do
  begin
    S := '';
    for IX := 0 to Pred(W) do
      S += M[IX, IY];
    WriteLn(F, S);
  end;
  CloseFile(F);
end;

procedure TForm1.SizeXEditingDone(Sender: TObject);
var
  NewValue: Integer;
begin
  if Integer.TryParse(SizeX.Text, NewValue) and (NewValue <> W) and (NewValue > 2) and (NewValue <= MaxSize) then
    W := NewValue;
  DrawMap;
end;

procedure TForm1.SizeYEditingDone(Sender: TObject);
var
  NewValue: Integer;
begin
  if Integer.TryParse(SizeY.Text, NewValue) and (NewValue <> H) and (NewValue > 2) and (NewValue <= MaxSize) then
    H := NewValue;
  DrawMap;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  IX, IY: Integer;
begin
  PaletteUndefined.Checked := true;
  W := 7;
  H := 7;
  for IX := 0 to Pred(MaxSize) do
    for IY := 0 to Pred(MaxSize) do
    begin
      B[IX, IY] := TMapItem.Create(Map);
      B[IX, IY].MX := IX;
      B[IX, IY].MY := IY;
      B[IX, IY].Parent := Map;
      B[IX, IY].Caption := '';
      B[IX, IY].OnMouseDown := @ClickBox;
    end;
  ClearMap;
  DrawMap;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  DrawMap;
end;

procedure TForm1.PaletteAnchorClick(Sender: TObject);
begin
  PaletteUndefined.Checked := false;
  PalettePass.Checked := false;
  PaletteAnchor.Checked := true;
  PaletteWall.Checked := false;
  PaletteTreasure.Checked := false;
end;

procedure TForm1.ButtonClearMapClick(Sender: TObject);
begin
  ClearMap;
  DrawMap;
end;

procedure TForm1.PaletteUndefinedClick(Sender: TObject);
begin
  PaletteUndefined.Checked := true;
  PalettePass.Checked := false;
  PaletteAnchor.Checked := false;
  PaletteWall.Checked := false;
  PaletteTreasure.Checked := false;
end;

procedure TForm1.PaletteWallClick(Sender: TObject);
begin
  PaletteUndefined.Checked := false;
  PalettePass.Checked := false;
  PaletteAnchor.Checked := false;
  PaletteWall.Checked := true;
  PaletteTreasure.Checked := false;
end;

procedure TForm1.PalettePassClick(Sender: TObject);
begin
  PaletteUndefined.Checked := false;
  PalettePass.Checked := true;
  PaletteAnchor.Checked := false;
  PaletteWall.Checked := false;
  PaletteTreasure.Checked := false;
end;

procedure TForm1.PaletteTreasureClick(Sender: TObject);
begin
  PaletteUndefined.Checked := false;
  PalettePass.Checked := false;
  PaletteAnchor.Checked := false;
  PaletteWall.Checked := false;
  PaletteTreasure.Checked := true;
end;

function TForm1.GetActiveChar: Char;
begin
  if PaletteUndefined.Checked then
    Exit('-')
  else
  if PalettePass.Checked then
    Exit('.')
  else
  if PaletteAnchor.Checked then
    Exit('+')
  else
  if PaletteWall.Checked then
    Exit('#')
  else
  if PaletteTreasure.Checked then
    Exit('$');
  raise Exception.Create('Unexpected palette state');
end;

function TForm1.GetColor(const C: Char): TColor;
begin
  case C of
    '-': Result := clSkyBlue;
    '.': Result := clGray;
    '+': Result := clBlue;
    '#': Result := clDefault;
    '$': Result := clYellow;
    else
      raise Exception.CreateFmt('Unexpected char: %s', [C]);
  end;
end;

procedure TForm1.ClickBox(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  MapItem: TMapItem;
begin
  MapItem := Sender as TMapItem;
  M[MapItem.MX, MapItem.MY] := GetActiveChar;
  if (M[MapItem.MX, MapItem.MY] = '.') and ((MapItem.MX = 0) or (MapItem.MY = 0) or (MapItem.MX = Pred(W)) or (MapItem.MY = Pred(H))) then
    M[MapItem.MX, MapItem.MY] := '+';
  if (M[MapItem.MX, MapItem.MY] = '.') or (M[MapItem.MX, MapItem.MY] = '$') or (M[MapItem.MX, MapItem.MY] = '+') then
  begin
    if (MapItem.MX - 1 >= 0) and (M[MapItem.MX - 1, MapItem.MY] = '-') then
      M[MapItem.MX - 1, MapItem.MY] := '#';
    if (MapItem.MX + 1 <= Pred(W)) and (M[MapItem.MX + 1, MapItem.MY] = '-') then
      M[MapItem.MX + 1, MapItem.MY] := '#';
    if (MapItem.MY - 1 >= 0) and (M[MapItem.MX, MapItem.MY - 1] = '-') then
      M[MapItem.MX, MapItem.MY - 1] := '#';
    if (MapItem.MY + 1 <= Pred(H)) and (M[MapItem.MX, MapItem.MY + 1] = '-') then
      M[MapItem.MX, MapItem.MY + 1] := '#';
  end;
  //MapItem.Caption := M[MapItem.MX, MapItem.MY];
  //MapItem.Color := GetColor(M[MapItem.MX, MapItem.MY]);
  DrawMap;
end;

procedure TForm1.DrawMap;
var
  IX, IY: Integer;
  BSize: Integer;
begin
  SizeX.Text := W.ToString;
  SizeY.Text := H.ToString;

  if Map.Width / W < Map.Height / H then
    BSize := Trunc(Map.Width / W)
  else
    BSize := Trunc(Map.Height / H);

  for IX := 0 to Pred(MaxSize) do
    for IY := 0 to Pred(MaxSize) do
    begin
      //if (M[IX, IY] = '.') and ((IX = 0) or (IY = 0) or (IX = Pred(W)) or (IY = Pred(H))) then
      //  M[IX, IY] := '+';
      //if (M[IX, IY] = '+') and not ((IX = 0) or (IY = 0) or (IX = Pred(W)) or (IY = Pred(H))) then
      //  M[IX, IY] := '.';
      B[IX, IY].Caption := M[IX, IY];
      B[IX, IY].Visible := (IX < W) and (IY < H);
      B[IX, IY].Color := GetColor(M[IX, IY]);
      B[IX, IY].Width := BSize;
      B[IX, IY].Height := BSize;
      B[IX, IY].Left := IX * BSize;
      B[IX, IY].Top := IY * BSize;
      if (IX = W div 2) or (IY = H div 2) or
         (not Odd(W) and (IX = W div 2 - 1)) or
         (not Odd(H) and (IY = H div 2 - 1)) then
        B[IX, IY].BevelWidth := 2
      else
        B[IX, IY].BevelWidth := 1;
    end;
end;

procedure TForm1.ClearMap;
var
  IX, IY: Integer;
begin
  for IX := 0 to Pred(MaxSize) do
    for IY := 0 to Pred(MaxSize) do
      M[IX, IY] := '-';
end;

end.

